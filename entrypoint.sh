#!/bin/bash

set -e

exec python3 src/openai_completion.py &
exec python3 src/openai_images.py &
exec python3 -u src/main.py
