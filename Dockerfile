FROM python:3.12.5-slim
WORKDIR discordbot
RUN apt-get update -y && apt-get install ffmpeg -y
COPY . .
RUN pip3 install -r requirements.txt --use-pep517
RUN chmod +x entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
