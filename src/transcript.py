
from app import App
from audio_extract import extract_audio
from io import BytesIO
from openai import OpenAI
from typing import Optional

import discord
import os
import pathlib
import requests
import shutil
import speech_recognition

class Transcript():
    recognizer = speech_recognition.Recognizer()
    allowed_users = ["radioaktivetaco", "zirka9", "samichoulo911"]
    path = f"{pathlib.Path(__file__).parent.resolve()}\\tmp"

    async def on_message(message: discord.Message) -> None:
        if message.author == App.client.user or message.author.name not in Transcript.allowed_users:
            return

        sent_message = await message.author.send("Processing... <:happyMyshka:1237542315535503481>")

        prompt: str = message.content
        if len(message.attachments):
            transcription = await Transcript.handle_audio_file(message.attachments[0], sent_message)
            if not transcription:
                return

            if len(message.content):
                prompt = f""" This is a transcription from an audio file.

                Transcription:
                {transcription}

                End of transcription

                Your response will be sent to the user that provided this transcript. A prompt will also be provided by the user,
                I want you to adapt your response based on the content of this prompt.

                Prompt:
                {message.content}
                """
            else:
                prompt = f"""
                    Your response must only consist of the corrected text.
                    Correct this text while keeping the same words: {transcription}
                """

        await Transcript.send_completion(prompt, message.author, sent_message)

    async def handle_audio_file(attachment: discord.Attachment, sent_message: discord.Message) -> Optional[str]:
        if "audio" in attachment.content_type:
            extension = attachment.filename.split('.')[-1].lower()

            Transcript.clear_existing_files()
            file = await Transcript.download_file(attachment.url, extension)
            transcription = await Transcript.transcribe_audio(file, sent_message)
            Transcript.clear_existing_files()

            return transcription
        return None

    async def transcribe_audio(audio_file: BytesIO, sent_message: discord.Message) -> Optional[str]:
        with speech_recognition.AudioFile(audio_file) as source:
            audio_data = Transcript.recognizer.record(source)
            try:
                return Transcript.recognizer.recognize_google(audio_data)
            except speech_recognition.UnknownValueError:
                await sent_message.edit(content="Could not understand audio")
                return None
            except speech_recognition.RequestError as e:
                await sent_message.edit(content=f"Could not request results from Google Speech Recognition service; {e}")
                return None
            except Exception as e:
                await sent_message.edit(content=f"An error occured; {e}")
                return None

    async def download_file(url: str, extension: str) -> str:
        response = requests.get(url, stream=True)

        local_path = f"{Transcript.path}\\audio.{extension}"
        os.makedirs(os.path.dirname(local_path), exist_ok=True)
        with open(local_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)

        output_path = f"{Transcript.path}\\audio.wav"

        if extension != "wav":
            extract_audio(input_path=local_path, output_path=output_path, output_format="wav", overwrite=True)

        return output_path

    async def send_completion(prompt: str, author: discord.User, sent_message: discord.Message):
        client = OpenAI(api_key=os.getenv("OPENAI_API_KEY", None))

        response = client.chat.completions.create(
            model="gpt-4o",
            messages=[{"role": "user", "content": prompt}],
            user=author.discriminator
        )

        messages = Transcript.split_text(response.choices[0].message.content.strip())
        await sent_message.edit(content=messages[0])
        for message in messages[1:]:
            await sent_message.channel.send(content=message)

    def split_text(text: str, chunk_size: int = 2000) -> list[str]:
        return [text[i:i + chunk_size] for i in range(0, len(text), chunk_size)]

    def clear_existing_files() -> None:
        if os.path.exists(Transcript.path):
            for filename in os.listdir(Transcript.path):
                file_path = os.path.join(Transcript.path, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason: %s' % (file_path, e))
