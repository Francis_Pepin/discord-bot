import aiohttp
import discord
import io
import json
import random
import re as Regex
import requests
import util

from app import App
from base_module import BaseModule
from bs4 import BeautifulSoup
from settings import Settings
from typing import Optional, final

class Image(BaseModule):
    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.author == App.client.user:
            return

        if restrictions["image"] or not restrictions["restricted"]:
            await Image.__send_image_with_search_term(message)
            await Image.__get_server_icon(message)

    # Based on https://codecondo.com/scrape-and-download-google-images-with-python/
    @staticmethod
    def fetch_image_urls_from_search_term(term, img_type, is_safe_search) -> list[str]:
        req_url = "https://www.google.com/search"
        params = {"q": term, "tbm": "isch"}
        headers = {
            "User-Agent":
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36"
        }

        if img_type == "gif":
            params["tbs"] = "itp:animated"

        if is_safe_search:
            params["safe"] = "active"

        html = requests.get(req_url, params=params, headers=headers)
        soup = BeautifulSoup(html.text, "lxml")

        all_script_tags = soup.select("script")
        images_data = "".join(Regex.findall(r"AF_initDataCallback\(([^<]+)\);", str(all_script_tags)))
        fixed_images_data = json.dumps(images_data)
        images_data_in_json = json.loads(fixed_images_data)

        matched_image_data = Regex.findall(r"\"b-GRID_STATE0\"(.*)sideChannel:\s?{}}", images_data_in_json)
        removed_matched_thumbnails = Regex.sub(
            r"\[\"(https\:\/\/encrypted-tbn0\.gstatic\.com\/images\?.*?)\",\d+,\d+\]",
            "",
            str(matched_image_data)
        )
        matched_resolution_images = Regex.findall(
            r"(?:’|,),\[\"(https:|http.*?)\",\d+,\d+\]",
            removed_matched_thumbnails
        )
        urls = [bytes(bytes(img, "ascii").decode("unicode-escape"), "ascii").decode("unicode-escape")
                for img in matched_resolution_images]

        return [urls[:len(urls) // 4], urls[-len(urls) // 4:]]

    def get_random_url(urls) -> Optional[str]:
        if not urls:
            return None
        else:
            chosen_list = random.choices(urls, weights=[8, 2], k=1)
            if chosen_list[0]:
                return random.choice(chosen_list[0])
            else:
                return None

    async def __send_image_with_search_term(message) -> None:
        if not (message.content.split(" ")[0] == "!image" or message.content.split(" ")[0] == "!gif"):
            return

        options = {
            "count": [1],
            "separator": (" ", 1),
            "replace": (0, "!", ""),
            "nb_params_lower": "You need to enter a search term! 🔍",
        }
        cmd = await util.command_async(message, options)

        img_type, term = cmd["command"].replace("!", ""), cmd["params"][0].replace("*", "")
        is_safe_search = Settings.info(message)["safe_search"]
        is_anonymous_fetch = message.content[-1] == "*"

        fetch_message = None
        if is_anonymous_fetch:
            await message.delete()
        else:
            fetch_message = await message.channel.send(f"Currently fetching {img_type} for {term}...")

        urls = Image.fetch_image_urls_from_search_term(term, img_type, is_safe_search)

        # Send url with a maximum of 3 attempts
        for _ in range(2):
            url = Image.get_random_url(urls)
            if url is None:
                return await fetch_message.edit(content="Nothing was found... Sorry!")

            if await Image.__send_image_from_url(url, message, img_type, term, fetch_message):
                return

        # If there is no valid url even after retries
        if not is_anonymous_fetch:
            await fetch_message.edit(content="We don't have anything for you... Sorry!")

    async def __send_image_from_url(url, message, content_type, term, fetch_message) -> None:
        is_valid_status = False
        async with aiohttp.ClientSession() as session, session.get(url) as resp:
            if is_valid_status := resp.status == 200:
                if fetch_message:
                    await fetch_message.edit(content=f"Here is your {content_type} for: {term}")
                data = io.BytesIO(await resp.read())
                filename = f"{term}.png" if content_type == "image" else f"{term}.gif"
                await message.channel.send(file=discord.File(data, filename))
            else:
                await message.edit(content="An error occured... Sorry!")
        return is_valid_status

    async def __get_server_icon(message) -> None:
        if message.content.strip() == "!server_icon":
            if message.guild.icon:
                async with aiohttp.ClientSession() as session, session.get(str(message.guild.icon)) as resp:
                    data = io.BytesIO(await resp.read())
                    await message.channel.send(file=discord.File(data, "server_icon.png"))
            else:
                await message.channel.send("This server has no icon!")
