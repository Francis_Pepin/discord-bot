import discord

from typing import Protocol

class BaseModule(Protocol):
    @staticmethod
    async def init() -> None:
        """Initialize the module."""
        pass

    @staticmethod
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        """Handle message events."""
        pass

    @staticmethod
    async def on_message_edit(before: discord.Message, after: discord.Message) -> None:
        """Handle message edit events."""
        pass

    @staticmethod
    async def on_voice_state_update(before: discord.VoiceState, after: discord.VoiceState, member: discord.Member) -> None:
        """Handle voice state update events."""
        pass

    @staticmethod
    async def on_raw_reaction_add(payload: discord.RawReactionActionEvent) -> None:
        """Handle raw reaction add events."""
        pass

    @staticmethod
    async def on_raw_reaction_remove(payload: discord.RawReactionActionEvent) -> None:
        """Handle raw reaction remove events."""
        pass
