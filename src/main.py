import asyncio
import discord
from dotenv import find_dotenv, load_dotenv

from app import App
from base_module import BaseModule
from cleanup import Cleanup
from config import Config
from daily_icon import DailyIcon
from emote import Emote
from help_command import Help
from image import Image
from music import Music
from odds import Odds
from openai_generator import OpenAIGenerator
from printer import Printer
from react import React
from restrictions import Restrictions
from role import Role
from settings import Settings
from spam import Spam
from transcript import Transcript
from uptime import Uptime
from urban_dictionary import UrbanDictionary
from wikipedia import Wikipedia

from typing import List, Type

registry: List[Type[BaseModule]] = [
    DailyIcon,
    Emote,
    Help,
    Image,
    Music,
    Uptime,
    React,
    Role,
    Odds,
    OpenAIGenerator,
    Settings,
    Spam,
    UrbanDictionary,
    Wikipedia,
    Cleanup
]

@App.client.event
async def on_ready() -> None:
    Printer.print_startup_info()

    Config.init()
    await asyncio.gather(
        *(module.init() for module in registry)
    )
    await App.tree.sync()
    App.is_ready = True
    Printer.print_confirmation("Beep Boop is ready to go!")

@App.client.event
async def on_message(message: discord.Message) -> None:
    # Disable functions for dms
    if not App.is_ready:
        return

    if not message.guild:
        return await Transcript.on_message(message)

    restrictions = await Restrictions.on_message(message)
    if not await Settings.is_channel_enabled(message, restrictions):
        return

    await asyncio.gather(
        *(module.on_message(message, restrictions) for module in registry)
    )

@App.client.event
async def on_message_edit(before: discord.Message, after: discord.Message) -> None:
    # Disable functions for dms
    if not before.guild:
        return

    await asyncio.gather(
        *(module.on_message_edit(before, after) for module in registry)
    )

@App.client.event
async def on_voice_state_update(member: discord.Member, before: discord.VoiceState, after: discord.VoiceState) -> None:
    await asyncio.gather(
        *(module.on_voice_state_update(before, after, member) for module in registry)
    )

@App.client.event
async def on_raw_reaction_add(payload: discord.RawReactionActionEvent) -> None:
    await asyncio.gather(
        *(module.on_raw_reaction_add(payload) for module in registry)
    )

@App.client.event
async def on_raw_reaction_remove(payload: discord.RawReactionActionEvent) -> None:
    await asyncio.gather(
        *(module.on_raw_reaction_remove(payload) for module in registry)
    )

load_dotenv(find_dotenv())
App.run()
