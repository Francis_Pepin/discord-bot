import os
import discord
import datetime as dt

MAX_MESSAGE_LENGTH = 2000
MAX_EMBED_LENGTH = 4096

def format_time(seconds: int, always_show_hours: bool = True):
    mins, secs = divmod(seconds, 60)
    hours, mins = divmod(mins, 60)
    days, hours = divmod(hours, 24)

    time = f"{int(mins):02d}m {int(secs):02d}s"
    if hours or always_show_hours:
        time = f"{int(hours):d}h " + time
    if days:
        time = f"{int(days):d}d " + time

    return time

def get_user_id(member: discord.User) -> str:
    return f"({member.id}) {get_user_key(member)}"

def get_user_key(member: discord.User) -> str:
    discriminator = f"#{member.discriminator}" if member.discriminator != "0" else ""
    return f"{member.name}{discriminator}"

# From https://stackoverflow.com/questions/61666851/python-get-datetime-of-next-occurring-time
def next_datetime(current: dt.datetime, hour: int, **kwargs) -> dt.datetime:
    repl = current.replace(hour=hour, **kwargs)
    while repl <= current:
        repl = repl + dt.timedelta(days=1)
    return repl

# Options:
# - count -> number of params
# - separator -> character separating the params with the number of separations
# - strip -> strip specified param
# - replace -> replace specified character with other character
# - nb_params_lower -> prints specified message if there are not enough params
# - nb_params_higher -> prints specified message if there are too many params
# - custom_content -> gives custom command content if called from somwhere other than the message
#
# Returns :
# - is_valid_params -> returns if the number of params is valid
# - nb_params -> returns number of extracted params
# - params -> returns the extracted params from the message
# - command -> returns the command name
def command(message: discord.Message, options: dict = {}) -> dict:
    is_valid_params = True
    params = []
    command_name = ""

    if "nb_params_lower" in options:
        if not options["nb_params_lower"] or options["nb_params_lower"] == "default":
            options["nb_params_lower"] = "Not enough parameters!"
    if "nb_params_higher" in options:
        if not options["nb_params_higher"] or options["nb_params_higher"] == "default":
            options["nb_params_higher"] = "Too many parameters!"

    if "separator" in options:
        separator = options["separator"]
        is_tuple = isinstance(separator, tuple)

        command_text = message.content
        if "custom_content" in options:
            command_text = options["custom_content"]

        if is_tuple:
            params = command_text.split(separator[0], separator[1])
        else:
            params = command_text.split(separator)

        command_name = params[0]
        params.remove(command_name)

    if "count" in options:
        is_valid_params = len(params) in options["count"]

    if "strip" in options:
        strip_index = options["strip"]

        if strip_index < len(params):
            params[strip_index] = params[strip_index].strip()

    if "replace" in options:
        replace_options = options["replace"]

        if replace_options[0] < len(params):
            params[replace_options[0]] = params[replace_options[0]].replace(replace_options[1], replace_options[2])

    return {
        "is_valid_params": is_valid_params,
        "nb_params": len(params),
        "params": params,
        "command": command_name,
    }

async def command_async(message: discord.Message, options: dict = None) -> dict:
    cmd = command(message, options)
    if "nb_params" in cmd and not cmd["is_valid_params"]:
        if "nb_params_lower" in options and cmd["nb_params"] < min(options["count"]):
            await message.channel.send(options["nb_params_lower"])
        elif "nb_params_higher" in options and cmd["nb_params"] > max(options["count"]):
            await message.channel.send(options["nb_params_higher"])
    return cmd

async def send_table(
        message: discord.Message,
        title: str,
        table_rows: str,
        is_double_space: bool = False,
        row_size: int = None) -> None:
    messages = []
    rows = table_rows.split("\n")

    line = "".join(["-" for _ in range(row_size if row_size else len(max(rows)) * 2)])
    fill_length = int((len(line) - len(title)) / 2)
    list_message = "".join([" " for _ in range(int(fill_length * 1.4))]) + title
    list_message = f"{line}\n{list_message}\n{line}\n"

    for line_text in rows:
        table_line = f"{line_text}\n"
        if line_text != rows[len(rows) - 1] and is_double_space:
            table_line += "\n"

        if len(list_message + table_line) < MAX_MESSAGE_LENGTH:
            list_message += table_line
        else:
            messages.append(list_message)
            list_message = table_line

    if len(list_message + line) < MAX_MESSAGE_LENGTH:
        list_message += line
        messages.append(list_message)
    else:
        messages.append(list_message)
        message.append(line)

    for msg in messages:
        await message.channel.send(msg)

def swap_array_items(array: list, first_index: int, second_index: int) -> list:
    array[first_index], array[second_index] = array[second_index], array[first_index]
    return array

def read_dict(dict: dict) -> str:
    content = ""
    for key, value in dict.items():
        if key != "_id":
            content += f"{key}: **{value}**\n"
    return content

def str_to_bool(string: str) -> bool:
    return string.lower() in ("yes", "true", "t", "1")
