import asyncio
import discord
import threading
import util

from app import App
from base_module import BaseModule
from typing import final

class Spam(BaseModule):
    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.author == App.client.user:
            return

        if restrictions["spam"] or not restrictions["restricted"]:
            await Spam.custom_spam(message)
            await Spam.timer(message, App.client)

    async def custom_spam(message: discord.Message) -> None:
        command_name = message.content.split(" ")[0]
        if not (command_name == "!spam" or command_name == "!tts"):
            return

        options = {
            "count": [1, 2],
            "separator": ("|", 2) if "|" in message.content else (" ", 2),
            "nb_params_lower": "Invalid syntax! Not enough parameters! 🚑",
            "nb_params_higher": "Invalid syntax! Too many parameters! 🚑",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        nb_messages = 3
        if cmd["nb_params"] == 2:
            nb_messages_param = int(cmd["params"][1])
            if nb_messages_param:
                nb_messages = min(max(1, nb_messages_param), 20)

        is_tts = cmd["command"] == "!tts"
        for _ in range(nb_messages):
            await message.channel.send(cmd["params"][0], tts=is_tts, delete_after=30 if is_tts else None)

        await message.delete()

    async def timer(message: discord.Message, client: discord.Client) -> None:
        timer_commands = ["!stimer", "!mtimer", "!htimer"]
        if not (message.content.split(" ")[0] in timer_commands):
            return

        options = {
            "count": [2],
            "separator": (" ", 2),
            "nb_params_lower": "You need to put an amount of time, as well as a message! ⛏️",
            "nb_params_higher": "??????????",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        try:
            time = int(cmd["params"][0])
        except BaseException:
            return await message.channel.send("Your first parameter needs to be a valid number! 💯")

        if cmd["command"][1] == "m":
            time *= 60
        elif cmd["command"][1] == "h":
            time *= 3600

        # Maximum of 24 hours
        if time > 86400:
            return await message.channel.send("The maximum amount of time possible is 24h! 🌺")

        threading.Timer(time, Spam.__send_timer_message_sync, (message, client, cmd["params"][1])).start()

    def __send_timer_message_sync(message: discord.Message, client: discord.Client, text: str) -> None:
        asyncio.run_coroutine_threadsafe(Spam.__send_timer_message(message, text), client.loop)

    async def __send_timer_message(message: discord.Message, text: str) -> None:
        await message.channel.send(text)
