import openai
import os

from util import str_to_bool
from pymongo import MongoClient

class Config:
    client: MongoClient
    openai_instance: openai

    @staticmethod
    def init() -> None:
        url = os.environ.get("MONGODB_URL", None)
        db_username = os.getenv("MONGODB_USERNAME", None)
        db_password = os.getenv("MONGODB_PASSWORD", None)

        Config.client = MongoClient(url, tls=True, username=db_username, password=db_password)

        # openai.organization = "org-sBHKsnKZdRcheS8EoHtb6lD1"
        openai.api_key = os.getenv("OPENAI_API_KEY", None)
        Config.openai_instance = openai

    @staticmethod
    def get_token() -> None:
        is_prod = str_to_bool(os.environ.get("IS_PROD", "False"))
        return os.environ.get("BOT_TOKEN" if is_prod else "DEV_TOKEN", None)

    @staticmethod
    def mongo_client(guild_id: str) -> MongoClient:
        return Config.client[guild_id]

    @staticmethod
    def get_openai() -> openai:
        return Config.openai_instance

    @staticmethod
    def is_supporting_emojis():
        return os.environ.get("IS_SUPPORTING_EMOJIS", False)
