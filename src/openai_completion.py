import asyncio
import config
import openai
import os
import rpyc
import sys

from openai import OpenAI
from printer import Printer
from rpyc.utils.server import ThreadedServer

class CompletionService(rpyc.Service):
    def __init__(self):
        config

    def exposed_redeploy(self):
        print("Killing completion generator")
        return sys.exit()

    def exposed_completion(self, prompt, author, temperature):
        return asyncio.run(self.internal_completion(prompt, author, temperature))

    async def internal_completion(self, prompt, author, temperature):
        try:
            client = OpenAI(
                api_key=os.getenv("OPENAI_API_KEY", None)
            )
            response = client.chat.completions.create(
                model="gpt-3.5-turbo",
                messages=[{"role": "user", "content": prompt}],
                max_tokens=300,
                temperature=temperature,
                user=author
            )

            return response.choices[0].message.content.strip()
        except Exception as e:
            print(e)
            content = "Couldn't generate an answer! 💥"
            match type(e):
                case openai.AuthenticationError:
                    content = "The bot couldn't be athentificated, please notify the bot's owner! 💥"
                case openai.BadRequestError:
                    content = "This command was rejected due to a bad request, please notify the bot's owner! 💥"
                case openai.BadRequestError:
                    content = "This command was rejected due to permission issues, please notify the bot's owner! 💥"

            return content


if __name__ == "__main__":
    Printer.print_confirmation("OpenAI completion server is running!")
    server = ThreadedServer(CompletionService, port=18813)
    server.start()
