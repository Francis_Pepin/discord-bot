import discord
import util

from app import App
from base_module import BaseModule
from config import Config
from emote import Emote
from react import React
from typing import final
from util import str_to_bool

class Settings(BaseModule):
    local_settings = {}
    bool_settings = ["safe_search", "dm_help_messages", "change_server_icon_daily", "enable_specific_channels"]
    float_settings = ["prompt_risk_factor"]

    @final
    async def init() -> None:
        for guild in App.client.guilds:
            Settings.__init_settings(guild.id)

    @staticmethod
    def info(message: discord.Message) -> dict:
        return Settings.__init_settings(message)

    @staticmethod
    def get_settings(guild_id: int) -> dict:
        return Settings.__init_settings(guild_id)

    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        await Settings.change_settings(message, restrictions)
        await Settings.enable_channel(message, restrictions)
        await Settings.disable_channel(message, restrictions)

    @staticmethod
    async def is_channel_enabled(message: discord.Message, restrictions: dict) -> bool:
        if Settings.local_settings[str(message.guild.id)]["enable_specific_channels"]:
            db = Config.mongo_client(str(message.guild.id))
            if not len(list(db.enabled_channels.find())):
                return True

            if not db.enabled_channels.find_one({"channel_id": message.channel.id}):
                Emote.count_emotes(message)
                await React.react_to_message(message)

                # In case we want to enable a channel currently disabled when enable_specific_channels is on
                await Settings.enable_channel(message, restrictions)
                return False

        return True

    async def change_settings(message: discord.Message, restrictions: dict) -> None:
        if not message.content.split(" ")[0] == "!settings" or \
                (not restrictions["settings"] and restrictions["restricted"]):
            return

        settings_dict = Settings.__init_settings(message)
        if message.content == "!settings":
            readable_settings = util.read_dict(settings_dict)
            return await message.channel.send(f"Settings in guild __{message.guild.name}__:\n{readable_settings}")

        options = {
            "count": [2],
            "separator": " ",
            "nb_params_lower": "Invalid format! Type !help for more info. ⚙️",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        setting: str = cmd["params"][0]
        value: str | float = cmd["params"][1]

        # Setting names
        guild_id = str(message.guild.id)
        if setting in Settings.bool_settings:
            value = str_to_bool(value)
        elif setting in Settings.float_settings:
            value = await Settings.__change_float_setting(message, value)
        else:
            return await message.channel.send("This setting doesn't exist! ⚙️")

        if value is None:
            return

        db = Config.mongo_client(guild_id)
        db_entry = db.settings.find_one()
        if db_entry:
            db.settings.update_one(
                {"_id": db_entry["_id"]},
                {'$set': {setting: value}},
                upsert=True
            )

            await message.channel.send(f"Setting **{setting}** updated to **{value}**! ⚙️")
            Settings.local_settings[guild_id][setting] = value

        return Settings.local_settings[guild_id]

    async def enable_channel(message: discord.Message, restrictions: dict) -> None:
        is_restricted = not restrictions["settings"] and restrictions["restricted"]
        if message.content.strip() != "!enable_channel" or is_restricted:
            return

        db = Config.mongo_client(str(message.guild.id))
        if db.enabled_channels.find_one({"channel_id": message.channel.id}):
            return await message.channel.send("This channel is already enabled! 🍗")

        db.enabled_channels.insert_one({"channel_id": message.channel.id})
        await message.channel.send(f"This channel has been enabled! 🥓")

    async def disable_channel(message: discord.Message, restrictions: dict) -> None:
        is_restricted = not restrictions["settings"] and restrictions["restricted"]
        if message.content.strip() != "!disable_channel" or is_restricted:
            return

        db = Config.mongo_client(str(message.guild.id))
        if not db.enabled_channels.find_one({"channel_id": message.channel.id}):
            return await message.channel.send("This channel is already disabled! 🦃")

        db.enabled_channels.delete_one({"channel_id": message.channel.id})
        await message.channel.send(f"This channel has been disabled! 🍁")

    async def __change_float_setting(message: discord.Message, value: str) -> float:
        try:
            returned_value = float(value)
            if not (returned_value >= 0.0 and returned_value <= 1.0):
                raise Exception
        except BaseException:
            await message.channel.send("You need to provide a value between 0 and 1 for this setting! ⚙️")
            return None

        return returned_value

    def __init_settings(id_source: discord.Message | int) -> dict:
        guild_id = str(id_source.guild.id) if isinstance(id_source, discord.Message) else str(id_source)
        if guild_id in Settings.local_settings:
            return Settings.local_settings[guild_id]

        mongo_client = Config.mongo_client(guild_id)
        db_settings = mongo_client.settings.find_one()
        local_settings = Settings.__default_settings()
        if not db_settings:
            mongo_client.settings.insert_one(local_settings)

        local_settings.update(db_settings)
        Settings.local_settings[guild_id] = local_settings

        return Settings.local_settings[guild_id]

    def __default_settings() -> dict:
        return {
            "safe_search": True,
            "dm_help_messages": False,
            "change_server_icon_daily": False,
            "enable_specific_channels": False,
            "prompt_risk_factor": 0.2,
        }
