import asyncio
import discord
import json
import pathlib
import random
import sys
import util

from app import App
from base_module import BaseModule
from discord import FFmpegPCMAudio
from typing import final

class Odds(BaseModule):
    file_path = pathlib.Path(__file__).parent.resolve().as_posix()
    delay = 60
    param_to_function: dict = {}
    custom_data = None

    @final
    async def init():
        with open(f"{Odds.file_path}/data/custom_data.json", "r", encoding="utf8") as file:
            data = json.loads(file.read())
            Odds.custom_data = data

        Odds.param_to_function = {
            "kick": Odds.punish_kick,
            "ban": Odds.punish_ban,
            "disconnect": Odds.punish_disconnect,
            "rename": Odds.punish_rename,
            "mute": Odds.punish_mute,
            "deafen": Odds.punish_deafen,
            "mix": Odds.punish_mix
        }

    @final
    async def on_message(message: discord.Message, _) -> None:
        author = message.author
        if author == App.client.user:
            return

        command = message.content.split(" ")[0]
        if command in ["!rr", "!russian_roulette"]:
            await Odds.__play_roulette(message, author)
        elif command == "!random":
            await Odds.__get_random_number(message)
        elif message.content.split("|")[0].strip() == "!choose":
            await Odds.__choose_outcome(message, author)

    async def __play_roulette(message: discord.Message, author: discord.User) -> None:
        options = {
            "count": [1],
            "separator": " ",
            "nb_params_lower": "You need to put something at stake! 🎰",
            "nb_params_higher": "Ayyy yoooo too many parameters here! 🎰",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        punish_param = cmd["params"][0]
        if punish_param == "list":
            info = "Use !rr or !russian_roulette with a punishement param: kick, ban, disconnect, rename, " + \
                "deafen, mute, mix\nEx: !rr rename"
            return await message.channel.send(info)

        punish_function = Odds.param_to_function.get(punish_param, None)
        if punish_function is None:
            return await message.channel.send(
                f"{author.display_name}, your punishement isn't even valid... <a:dinkdonk:885523468760805376>"
            )

        if random.randint(1, 6) == 1:
            await message.channel.send(f"{author.display_name} has been shot! 🔫")
            Odds.__play_audio(message, "bang.wav")
            return await punish_function(message, author)
        else:
            Odds.__play_audio(message, "empty.wav")
            return await message.channel.send(f"{author.display_name} is safe for now... <:monkaSpeed:894675694699823165>")

    async def __get_random_number(message: discord.Message) -> None:
        options = {
            "count": [0, 1, 2],
            "separator": " ",
            "nb_params_higher": "Ayyy yoooo too many parameters here! 🐙",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        try:
            from_param = int(cmd["params"][0]) if cmd["nb_params"] == 2 else 1
            to_param = int(cmd["params"][cmd["nb_params"] - 1]) if cmd["nb_params"] > 0 else 10
        except BaseException:
            return await message.channel.send("Parameters are invalid! 🥵")

        if max(from_param, to_param) > sys.maxsize or min(from_param, to_param) < -sys.maxsize - 1:
            return await message.channel.send("Parameters are out of range! 🥵")
        if from_param > to_param:
            return await message.channel.send("The first parameter has to be lower than the second! 🥵")

        return await message.channel.send(f"{random.randint(from_param, to_param)}")

    async def __choose_outcome(message: discord.Message, author: discord.User) -> None:
        options = {
            "count": range(2, 30),
            "separator": "|",
            "nb_params_lower": "You need to choose at least 2 outcomes for Beep Boop to decide! 🥝",
            "nb_params_higher": "Ayyy yoooo too many parameters here! Choose between 2 and 30 outcomes! 🐙",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        custom_message = random.sample(Odds.custom_data["decisions"], 1)[0].replace("{}", f"{author.nick}")
        decision = random.sample(cmd["params"], 1)[0].strip()
        return await message.channel.send(f"{custom_message}{decision}")

    # Punishements
    async def punish_kick(message: discord.Message, author: discord.User) -> None:
        await author.kick()

    async def punish_ban(message: discord.Message, author: discord.User) -> None:
        await author.ban()

    async def punish_disconnect(message: discord.Message, author: discord.User) -> None:
        await author.move_to(None)

    async def punish_rename(message: discord.Message, author: discord.User) -> None:
        initial_name = author.nick
        await author.edit(nick=random.sample(Odds.custom_data["nicknames"], 1)[0])
        await Odds.__reset_nickname(message, initial_name, author)

    async def punish_mute(message: discord.Message, author: discord.User) -> None:
        await author.edit(mute=True)
        await Odds.__reset_audio(message, author)

    async def punish_deafen(message: discord.Message, author: discord.User) -> None:
        await author.edit(deafen=True)
        await Odds.__reset_audio(message, author)

    async def punish_mix(message: discord.Message, author: discord.User) -> None:
        await Odds.punish_deafen(message, author)
        await Odds.punish_rename(message, author)
        await Odds.punish_mute(message, author)

    # Reset status
    async def __reset_audio(message: discord.Message, author: discord.User) -> None:
        await asyncio.sleep(Odds.delay)
        await author.edit(mute=False, deafen=False)
        await message.channel.send(f"{author.nick} is back from the dead! <:pepePoint:886083864576016436>")
        Odds.__play_audio(message, "AUGH.wav")

    async def __reset_nickname(message: discord.Message, name: str, author: discord.User) -> None:
        await asyncio.sleep(Odds.delay)
        await author.edit(nick=name)
        await message.channel.send(f"{author.nick} is back from the dead! <:pepePoint:886083864576016436>")
        Odds.__play_audio(message, "AUGH.wav")

    def __play_audio(message: discord.Message, file_name: str) -> None:
        if message.guild.voice_client and not message.guild.voice_client.is_playing():
            path = f"{Odds.file_path}/data/_sounds/{file_name}"
            audio = FFmpegPCMAudio(source=path)
            message.guild.voice_client.play(audio)
