import discord
import pathlib
import re as Regex
import util

from app import App
from base_module import BaseModule
from settings import Settings
from typing import final

class Help(BaseModule):
    keys: list[str] = None
    content: list[str] = None

    @final
    async def init() -> None:
        path = pathlib.Path(__file__).parent.resolve()
        local_file_path = path / "data/help.txt"
        with open(local_file_path, "r", encoding="utf-8") as file:
            text = file.read()

        Help.keys = Regex.findall(r'\%(.*?)\%', text)
        Help.content = Regex.sub(r'\%(.*?)\%', '', text).split("&")

    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.author == App.client.user or not message.content == "!help":
            return

        settings = Settings.info(message)
        await Help.__send_commands(message, restrictions, settings["dm_help_messages"])

    async def __send_commands(message: discord.Message, restrictions: dict, is_dm_message: bool) -> None:
        custom_help_messages: list[str] = []
        current_message: str = ""

        for key, content in zip(Help.keys, Help.content):
            if key == "valid" or not restrictions["restricted"] or restrictions[key]:
                if len(current_message + content) < util.MAX_MESSAGE_LENGTH:
                    current_message += content
                else:
                    custom_help_messages.append(current_message)
                    current_message = content

        if current_message:
            custom_help_messages.append(current_message)

        send_method = message.author.send if is_dm_message else message.channel.send

        for help_message in custom_help_messages:
            await send_method(help_message)
