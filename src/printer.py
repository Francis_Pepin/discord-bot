from config import Config

from app import App
from discord import Enum

class Printer():
    class TerminalColors(Enum):
        MAGENTA = "\033[95m"
        CYAN = "\033[96m"
        BLUE = "\033[94m"
        GREEN = "\033[92m"
        YELLOW = "\033[93m"
        RED = "\033[91m"
        HEADER = "\033[95m"
        ENDC = "\033[0m"
        BOLD = "\033[1m"
        UNDERLINE = "\033[4m"

    @staticmethod
    def print_rainbow_text(text: str) -> None:
        i, result = 0, ""
        terminal_colors = list(Printer.TerminalColors)[0:6]
        for n in range(0, len(text)):
            result += f"{terminal_colors[i % len(terminal_colors)].value}{text[n]}{Printer.TerminalColors.ENDC.value}"

            if not (text[n] == "\n" or text[n] == " "):
                i += 1

        print(result)

    @staticmethod
    def print_startup_info() -> None:
        ascii_text = """
            ____                     ____
           / __ )___  ___  ____     / __ )____  ____  ____
          / __  / _ \\/ _ \\/ __ \\   / __  / __ \\/ __ \\/ __ \\
         / /_/ /  __/  __/ /_/ /  / /_/ / /_/ / /_/ / /_/ /
        /_____/\\___/\\___/ .___/  /_____/\\____/\\____/ .___/
                       /_/                        /_/
        """

        ascii_lines = ascii_text.split("\n")
        colors = list(Printer.TerminalColors)[0:6]
        for i, line in enumerate(ascii_lines):
            color_index = i % len(colors)
            Printer.printc(line, colors[color_index])

        Printer.print_rainbow_text(
            "\nA discord bot made by Francis Pepin available at https://gitlab.com/Francis_Pepin/discord-bot\n")
        Printer.print_confirmation(f"Beep Boop has been activated on instance : {App.client.user}!", "🔌")

    @staticmethod
    def print_confirmation(text: str, custom_emoji: str = None) -> None:
        emoji = custom_emoji if custom_emoji else "✔️ "
        text = f"{emoji} {text} {emoji}" if Config.is_supporting_emojis() else text
        Printer.printc(text, Printer.TerminalColors.GREEN)

    @staticmethod
    def print_warning(text: str, custom_emoji: str = None) -> None:
        emoji = custom_emoji if custom_emoji else "⚠️"
        text = f"{emoji}  {text} {emoji}" if Config.is_supporting_emojis() else text
        Printer.printc(text, Printer.TerminalColors.YELLOW)

    @staticmethod
    def print_error(text: str, custom_emoji: str = None) -> None:
        emoji = custom_emoji if custom_emoji else "❌"
        text = f"{emoji}  {text} {emoji}" if Config.is_supporting_emojis() else text
        Printer.printc(text, Printer.TerminalColors.RED)

    @staticmethod
    def printc(text: str, color: TerminalColors) -> None:
        print(f"{color.value}{text}{Printer.TerminalColors.ENDC.value}")
