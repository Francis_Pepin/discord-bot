import discord
import emoji
import util

from app import App
from base_module import BaseModule
from config import Config
from pymongo import MongoClient
from pymongo.collection import Collection
from typing import final

class Emote(BaseModule):
    @final
    async def on_message(message: discord.Message, _) -> None:
        if message.author == App.client.user:
            return

        Emote.count_emotes(message)

        await Emote.__show_emote_in_message_counts(message)

    @final
    async def on_raw_reaction_add(payload: discord.RawReactionActionEvent) -> None:
        if payload.user_id == App.client.user.id:
            return

        await Emote.__update_reaction_count(payload, 1)

    @final
    async def on_raw_reaction_remove(payload: discord.RawReactionActionEvent) -> None:
        if payload.user_id == App.client.user.id:
            return

        await Emote.__update_reaction_count(payload, -1)

    async def __update_reaction_count(payload: discord.RawReactionActionEvent, increment: int) -> None:
        db = Config.mongo_client(str(payload.guild_id))
        emoji_id = str(payload.emoji)
        emote_data = db.reaction_counts.find_one({"id": emoji_id})

        if emote_data:
            count = emote_data["count"] + increment
        else:
            count = 0 if increment < 0 else 1

        db.reaction_counts.replace_one(
            {"id": emoji_id},
            {"id": emoji_id, "count": max(count, 0)},
            upsert=True
        )

    async def __show_emote_in_message_counts(message: discord.Message) -> None:
        if not message.content.split(" ")[0] == "!stats":
            return

        options = {
            "count": [1],
            "separator": " ",
            "strip": 0,
            "nb_params_lower": "default",
            "nb_params_higher": "default"
        }
        cmd = await util.command_async(message, options)

        global_emojis = "".join(c for c in message.content if emoji.is_emoji(c))

        params = cmd["params"]
        if cmd["params"][0] == "emotes":
            await Emote.__full_emote_count(message, False)
        elif cmd["params"][0] == "reacts":
            await Emote.__full_emote_count(message, True)
        elif message.content.count("<") == 1 or global_emojis:
            db = Config.mongo_client(str(message.guild.id))

            emote_count = db.emote_counts.find_one({"id": params[0]})
            react_count = db.reaction_counts.find_one({"id": params[0]})

            result = ""
            if emote_count and emote_count["count"] > 0:
                result += f"Emote {params[0]} has been typed: **{emote_count['count']} times**\n"
            if react_count and react_count["count"] > 0:
                result += f"Emote {params[0]} has been reacted: **{react_count['count']} times**"
            if not (emote_count or react_count):
                result += "The emote hasn't been used or doesn't exist! 🤔"

            await message.channel.send(result)

    async def __full_emote_count(message: discord.Message, is_reaction_collection: bool) -> None:
        table_rows = ""
        db = Config.mongo_client(str(message.guild.id))
        server_emojis = [emoji.id for emoji in message.guild.emojis]

        collection = db.reaction_counts if is_reaction_collection else db.emote_counts
        for emote in collection.find().sort([("count", -1), ("id", 1)]):
            emote_id = emote["id"]
            is_custom_emoji = ":" in emote["id"]

            if is_custom_emoji:
                emote_id = int(emote["id"].split(":")[2].replace(">", ""))

            if (emote_id in server_emojis or not is_custom_emoji) and emote["count"] > 0:
                table_rows += f"{emote['id']}: **{emote['count']} times**\n"

        title = "Reaction counts" if is_reaction_collection else "Emote counts"
        await util.send_table(message, title, table_rows.strip(), row_size=30)

    @staticmethod
    def count_emotes(message: discord.Message) -> None:
        # Not counting emotes when fetching stats
        if message.content.split(" ")[0] == "!stats":
            return

        # Find custom emotes in a message
        mongo_client = Config.mongo_client(str(message.guild.id))
        for emote in message.guild.emojis:
            emoji_text = f"{emote.name}:{emote.id}"
            if (emoji_text in message.content):
                Emote.__update_emote_data(mongo_client, str(emote))

        # Extract all the unicode emojis
        # https://stackoverflow.com/questions/43146528/how-to-extract-all-the-emojis-from-text
        global_emojis = "".join(c for c in message.content if emoji.is_emoji(c))
        for global_emoji in global_emojis:
            Emote.__update_emote_data(mongo_client, global_emoji)

    def __update_emote_data(db: MongoClient, emoji_id: str) -> None:
        emote_collection = db.emote_counts.find_one({"id": emoji_id})
        if emote_collection:
            Emote.__update_emote_count(emoji_id, emote_collection, db)
        else:
            Emote.__insert_emote_count(emoji_id, db)

    def __update_emote_count(emoji_id: str, emote_collection: Collection, mongo_client: MongoClient) -> None:
        mongo_client.emote_counts.replace_one(
            {"id": emoji_id},
            {"id": emoji_id, "count": emote_collection["count"] + 1},
            upsert=False
        )

    def __insert_emote_count(emoji_id, db) -> None:
        db.emote_counts.insert_one({"id": emoji_id, "count": 1})
