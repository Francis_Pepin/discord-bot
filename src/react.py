import discord
import util

from app import App
from base_module import BaseModule
from config import Config
from typing import final

class React(BaseModule):
    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.author == App.client.user:
            return

        if restrictions["react"] or not restrictions["restricted"]:
            await React.__add_custom_react(message)
            await React.__delete_term(message)

        await React.react_to_message(message)
        await React.__react_list(message)

    @final
    async def on_message_edit(before: discord.Message, after: discord.Message) -> None:
        if before.content == after.content:
            return

        # Remove all reactions only the bot has reacted
        for reaction in before.reactions:
            if reaction.me and reaction.count == 1:
                await before.clear_reactions()
                break

        await React.__react_to_message(after)

    @staticmethod
    async def react_to_message(message: discord.Message) -> None:
        await React.__default_react(message)
        await React.__react_to_message(message)

    async def __default_react(message: discord.Message) -> None:
        if message.content.split(" ")[0] == "!react_add":
            return

        emojis = message.guild.emojis
        edited_message = message.content.lower().replace(" ", "")
        for emoji in emojis:
            if emoji.name.lower() in edited_message:
                await message.add_reaction(emoji)

    async def __add_custom_react(message: discord.Message) -> None:
        if not message.content.split(" ")[0] == "!react_add":
            return

        options = {
            "count": [2],
            "separator": ("|", 2),
            "strip": 0,
            "replace": (1, " ", ""),
            "nb_params_lower": "You need to enter a term/phrase and a list of emojis to react! 🎈",
            "nb_params_higher": "Yo chill, there are too many parameters... 🎈"
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        term, emojis = cmd["params"][0], cmd["params"][1]

        try:
            custom_emoji = ""
            emoji_list = []
            for symbol in emojis:
                if symbol == "<" or custom_emoji:
                    custom_emoji += symbol

                    if symbol == ">":
                        emoji_list.append(custom_emoji)
                        custom_emoji = ""
                else:
                    emoji_list.append(symbol)

            for emoji in emoji_list:
                await message.add_reaction(emoji)
        except Exception:
            return await message.channel.send("Invalid react command structure! ✂️")

        db = Config.mongo_client(str(message.guild.id))
        result = db.term_emojis.replace_one(
            {"term": term},
            {"term": term, "emojis": emoji_list},
            upsert=True
        )
        suffix = "updated! 🤲" if result.raw_result["updatedExisting"] else "added! 🖨️"
        await message.channel.send(f"Emojis for *{term}* have been {suffix}")

    async def __react_list(message: discord.Message) -> None:
        if message.content != "!react_list":
            return

        table_rows = ""
        db = Config.mongo_client(str(message.guild.id))
        for term_emoji in db.term_emojis.find().sort("term"):
            table_rows += f"• {term_emoji['term']} : {''.join(str(emoji) + ' ' for emoji in term_emoji['emojis'])}\n"

        await util.send_table(message, "Term/emojis", table_rows.strip(), row_size=65)

    async def __react_to_message(message) -> None:
        if "!image" in message.content and "*" in message.content:
            return

        db = Config.mongo_client(str(message.guild.id))
        react_list = []

        for term_emoji in db.term_emojis.find():
            if (term_emoji['term'] in message.content):
                # Create tuple from index and reaction
                index = message.content.find(term_emoji['term'])
                react_list.append((index, term_emoji['emojis']))

        # Order according to word occurence index and react
        react_list.sort(key=lambda tup: tup[0])
        for emoji_tuple in react_list:
            for emoji in emoji_tuple[1]:
                await message.add_reaction(emoji)

    async def __delete_term(message: discord.Message) -> None:
        if not message.content.split(" ")[0] == "!react_delete":
            return

        try:
            options = {
                "count": [1],
                "separator": (" ", 1),
                "nb_params_lower": "You need to enter a term/phrase you want to delete! ⚗️",
                "nb_params_higher": "Yo chill, there are too many parameters... ⚗️"
            }
            cmd = await util.command_async(message, options)
            if not cmd["is_valid_params"]:
                return

            term = cmd["params"][0]
            db = Config.mongo_client(str(message.guild.id))

            if not list(db.term_emojis.find_one({"term": term})):
                return await message.channel.send("There are no emojis associated with this term! 🚽")

            db.term_emojis.delete_one({"term": term})
            await message.channel.send(f"*{term}* has been deleted! 🧹")
        except Exception:
            await message.channel.send("Not able to delete this term! 🚽")
