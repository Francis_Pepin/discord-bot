import discord
import requests
import util
import wikipediaapi

from app import App
from base_module import BaseModule
from discord import Colour, Embed
from typing import Optional, final

WIKI_API_URL = "https://en.wikipedia.org/w/api.php"

class Wikipedia(BaseModule):
    wiki_instance = wikipediaapi.Wikipedia("BeepBoop", "en")

    @final
    async def on_message(message: discord.Message, _) -> None:
        if message.author == App.client.user:
            return

        await Wikipedia.__fetch_term_definition(message)

    async def __fetch_term_definition(message: discord.Message) -> None:
        if not (message.content.split(" ")[0] == "!wiki"):
            return

        if (message.content == "!wiki"):
            title = Wikipedia.__generate_random_article_title()
            image = Wikipedia.__fetch_image_from_article_title(title)
            return await Wikipedia.__send_wiki_page(message, title, image)

        options = {
            "count": [1],
            "separator": (" ", 1),
            "nb_params_lower": "You need to enter a search term! 🔍",
        }
        cmd = await util.command_async(message, options)
        term = cmd["params"][0]

        title = Wikipedia.__extract_article_title(term)
        image = Wikipedia.__fetch_image_from_article_title(title)
        await Wikipedia.__send_wiki_page(message, title, image)

    def __extract_article_title(term: str) -> str:
        params = {
            "action": "query",
            "format": "json",
            "prop": "extracts",
            "srsearch": term,
            "list": "search",
            "exintro": True,
            "explaintext": True,
        }

        response = requests.get(WIKI_API_URL, params=params)
        data = response.json()

        if (not len(data["query"]["search"]) > 0):
            return None

        return data["query"]["search"][0]["title"]

    def __generate_random_article_title() -> str:
        params = {
            "action": "query",
            "format": "json",
            "prop": "extracts",
            "list": "random",
            "generator": "random",
            "rnnamespace": "0",
            "rnlimit": "1",
        }

        response = requests.get(WIKI_API_URL, params=params)
        data = response.json()

        if (not len(data["query"]["random"]) > 0):
            return None, None

        return data["query"]["random"][0]["title"]

    def __fetch_image_from_article_title(title: str) -> str:
        params = {
            "action": "query",
            "prop": "pageimages",
            "format": "json",
            "piprop": "original",
            "titles": title
        }

        response = requests.get(WIKI_API_URL, params=params)
        data = response.json()

        if ("query" not in data):
            return None

        # Get page key number
        key = list(data["query"]["pages"])[0]

        if ("original" not in data["query"]["pages"][key]):
            return None

        return data["query"]["pages"][key]["original"]["source"]

    async def __send_wiki_page(message: discord.Message, title: str, image: Optional[str]) -> str:
        if (not title):
            return await message.channel.send("No page was found, sorry!")

        try:
            page = Wikipedia.wiki_instance.page(title)

            embed = Embed(
                title=page.title[0:255],
                description=page.summary.replace("\n", "\n\n")[0:4095],
                url=page.fullurl,
                color=Colour.random()
            )

            if (image):
                embed.set_image(url=image)

            await message.channel.send(embed=embed)
        except BaseException:
            await message.channel.send("Couldn't retrieve the Wikipedia page!")
