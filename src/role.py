import discord
import re as Regex

from app import App
from base_module import BaseModule
from discord import Colour
from typing import Optional, final

class Role(BaseModule):
    r_data = {}
    r_id = 0

    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.author == App.client.user:
            return

        await Role.__get_role_list(message)

        if restrictions["restrict"] or not restrictions["restricted"]:
            await Role.__get_server_role_list(message)

        if restrictions["roles"] or not restrictions["restricted"]:
            await Role.__get_colors_list(message)
            await Role.__update_role_attributes(message)

    async def __get_role_list(message: discord.Message) -> None:
        if message.content != "!roles":
            return

        roles = [f"{i}. {role.name}\n" for i, role in enumerate(message.author.roles)]
        roles.pop(0)
        roles_list = f"Here are you roles :\n{''.join(roles)}"
        await message.channel.send(roles_list)

    async def __get_server_role_list(message: discord.Message) -> None:
        if message.content != "!server_roles":
            return

        roles = [f"- {role.name} `{role.id}`\n" for role in message.guild.roles]
        roles[0] = roles[0].replace("@", "")
        roles_list = f"Here are the server roles :\n{''.join(roles)}"
        await message.channel.send(roles_list)

    async def __get_colors_list(message: discord.Message) -> None:
        if message.content != "!colors":
            return

        colors = vars(Colour)
        colors = [f"{color}\n" for color in colors.keys() if not color.startswith("_") and
                  color not in ["r", "g", "b", "to_rgb", "from_rgb", "from_hsv", "value"]]
        colors_list = f"Here are the existing colors :\n{''.join(colors)}"
        await message.channel.send(colors_list)

    async def __update_role_attributes(message: discord.Message) -> None:
        commands = ["!role_name", "!role_color"]
        if not (message.content.split(" ")[0] == commands[0] or message.content.split(" ")[0] == commands[1]):
            return

        roles = message.author.roles
        roles.pop(0)

        words = message.content.split(" ", 2)

        try:
            command, role_name, new_param = words
            if command not in commands:
                return await message.channel.send("The command format is invalid! 🙃")
        except Exception:
            return await message.channel.send("The command format is invalid! 🙃")

        role_to_modify = await Role.__verify_params(message, roles, role_name)
        try:
            if command == commands[0]:
                await role_to_modify.edit(name=new_param)
                return await message.channel.send(f"Role name modified to: {new_param}! 🗿")
            elif command == commands[1]:
                if role_to_modify.id in Role.r_data:
                    Role.r_data.pop(role_to_modify.id)
                await Role.__handle_color_formats(role_to_modify, new_param)
                return await message.channel.send(f"Role {role_to_modify.name}'s color has been modified to {new_param}! 🎨")
        except Exception:
            return await message.channel.send(
                "Why are you trying to break the server? You just caused an exception with " +
                "whatever you typed, so like, change stuff...? 💣"
            )

    async def __verify_params(message: discord.Message, roles: list[discord.Role], role_name: str) -> Optional[discord.Role]:
        role_to_modify = None
        try:
            index = int(role_name)
            role_to_modify = roles[index - 1]
        except ValueError:
            return await message.channel.send("Please enter a valid index number! 🧮")
        except IndexError:
            return await message.channel.send("The role index does not exist! 🐱‍👤")

        return role_to_modify

    async def __handle_color_formats(role_to_modify: discord.Role, new_param: str) -> None:
        color = getattr(Colour, new_param, None)
        if color:
            return await role_to_modify.edit(colour=color())

        hex_text = new_param.lower().replace("#", "").replace("0x", "")
        hexadecimal_regex = Regex.compile(r"[0-9a-f]{6}")
        is_hex = hexadecimal_regex.search(hex_text)
        if is_hex:
            hex_color = int(hex(int(f"0x{hex_text}", 16)), 0)
            return await role_to_modify.edit(colour=hex_color)

        red, green, blue = new_param.split(" ", 2)
        rgb_color = Colour.from_rgb(int(red), int(green), int(blue))

        await role_to_modify.edit(colour=rgb_color)
