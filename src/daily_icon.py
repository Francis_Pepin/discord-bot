import aiohttp
import asyncio
import datetime as dt
import discord
import io
import mimetypes
import random
import threading
import util

from app import App
from base_module import BaseModule
from config import Config
from image import Image
from printer import Printer
from pytz import timezone
from settings import Settings
from typing import Optional, final

class DailyIcon(BaseModule):
    supported_extensions: list[str] = [".jpg", ".png"]

    @final
    async def init() -> None:
        time_zone = timezone('Canada/Eastern')
        for server in App.client.guilds:
            target_time = util.next_datetime(dt.datetime.now(time_zone), hour=4, minute=0, second=0)
            delay = (target_time - dt.datetime.now(time_zone)).seconds
            threading.Timer(delay, DailyIcon.change_icon_sync, (server, App.client)).start()

    def change_icon_sync(server: discord.Guild) -> None:
        asyncio.run_coroutine_threadsafe(DailyIcon.change_icon(server), App.client.loop)

    async def change_icon(server: discord.Guild) -> None:
        # Call function everyday
        day_in_seconds = 86400
        threading.Timer(day_in_seconds, DailyIcon.change_icon_sync, (server, App.client)).start()

        # Check if daily server icon setting is on
        server_settings = Settings.get_settings(server.id)
        if not server_settings["change_server_icon_daily"]:
            return

        terms = DailyIcon.__fetch_server_icon_terms(server.id)
        if not terms:
            return Printer.print_warning(f"Couldn't change icon for server {server.id} (empty list)")

        term = random.choice(terms)
        is_safe_search = Settings.get_settings(server.id)["safe_search"]

        urls = Image.fetch_image_urls_from_search_term(term, "image", is_safe_search)
        url = Image.get_random_url(urls)

        if img := await DailyIcon.__attempt_select_image(url):
            await server.edit(icon=img)

    async def __attempt_select_image(url) -> Optional[bytes]:
        async with aiohttp.ClientSession() as session, session.get(str(url)) as resp:
            if resp.status != 200:
                return None

            try:
                content_type = resp.headers['content-type']
                extension = mimetypes.guess_extension(content_type)

                if extension in DailyIcon.supported_extensions:
                    data = io.BytesIO(await resp.read())
                    return data.read()
                else:
                    return None
            except BaseException:
                return None

    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if message.content.strip() == "!icon_list":
            terms = DailyIcon.__fetch_server_icon_terms(message.guild.id)

            terms_string = ""
            for term in terms:
                terms_string += f"{term}\n"

            if terms:
                await util.send_table(message, "Server icon terms list", terms_string.strip(), row_size=64)
            else:
                await message.channel.send("No server icon terms have been added yet! 🍺")

        is_valid_command = "!icon_add" in message.content or "!icon_remove" in message.content
        has_access = restrictions["image"] or not restrictions["restricted"]
        if message.author == App.client.user or not has_access or not is_valid_command:
            return

        options = {
            "count": [1],
            "separator": (" ", 1),
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        guild_id = str(message.guild.id)
        db = Config.mongo_client(guild_id)
        if cmd["command"] == "!icon_add":
            await DailyIcon.__insert_server_icon_term(message, cmd["params"][0], db)
        elif cmd["command"] == "!icon_remove":
            await DailyIcon.__remove_server_icon_term(message, cmd["params"][0], db)

    async def __insert_server_icon_term(message: discord.Message, term: str, db) -> None:
        if db.server_icon_terms.find_one({"term": term}):
            return await message.channel.send("This icon term already exists! 🍂")

        db.server_icon_terms.insert_one({"term": term})
        await message.channel.send(f"The icon term {term} has been added! 🍒")

    async def __remove_server_icon_term(message, term, db) -> None:
        if not db.server_icon_terms.find_one({"term": term}):
            return await message.channel.send("This icon does not exist! 🥧")

        db.server_icon_terms.delete_one({"term": term})
        await message.channel.send(f"The icon term {term} has been deleted! 🏮")

    def __fetch_server_icon_terms(guild_id) -> list[str]:
        db = Config.mongo_client(str(guild_id))

        terms = []
        for entry in db.server_icon_terms.find().sort("term"):
            terms.append(entry["term"])

        return terms
