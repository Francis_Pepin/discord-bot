import discord
import subprocess

from config import Config
from discord import app_commands
from discord.app_commands import CommandTree

intents = discord.Intents.all()
class App:
    client: discord.Client = discord.Client(intents=intents)
    tree: CommandTree = app_commands.CommandTree(client)
    is_ready: bool = False

    def run() -> None:
        subprocess.Popen(["python", "src/openai_completion.py"])
        subprocess.Popen(["python", "src/openai_images.py"])

        token = Config.get_token()
        App.client.run(token)
