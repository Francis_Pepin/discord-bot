import discord
import util

from app import App
from config import Config
from typing import final

class Restrictions:
    local_restrictions = {}
    local_ids = []

    @final
    async def on_message(message: discord.Message) -> None:
        if message.author == App.client.user:
            return {
                "restricted": False,
                "restrict": True,
                "image": True,
                "react": True,
                "music": True,
                "spam": True,
                "roles": True,
                "settings": True,
                "cleanup": True,
            }

        role_ids = [str(role.id) for role in message.author.roles]
        db = Config.mongo_client(str(message.guild.id))
        current_restrictions = []
        for role_id in role_ids:
            if role_id not in Restrictions.local_ids:
                entry = db.restrictions.find_one({"id": role_id})

                role_name = message.guild.get_role(int(role_id)).name
                if not entry:
                    entry = Restrictions.__default_restrictions(role_id, role_name, False)
                    db.restrictions.insert_one(entry)

                if entry["name"] != role_name and role_name:
                    db.restrictions.update_one(
                        {"id": role_id},
                        {"$set": {"name": role_name}},
                        upsert=False
                    )
                Restrictions.local_ids.append(role_id)
                Restrictions.local_restrictions[role_id] = entry
            else:
                entry = Restrictions.local_restrictions[role_id]

            current_restrictions.append(entry)

        restrictions = Restrictions.__get_merged_restrictions(current_restrictions)

        await Restrictions.__restrictions_command(message, restrictions, current_restrictions)
        await Restrictions.__show_restrictions(message, restrictions)

        return restrictions

    async def __restrictions_command(message: discord.Message, user_restrictions: dict, current_restrictions: dict) -> None:
        if not message.content.split(" ")[0] == "!restriction" or not user_restrictions["restrict"]:
            return

        options = {
            "count": [3],
            "separator": "|",
            "nb_params_lower": "Invalid format! Type !help for more info. 🔐",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        restriction = cmd["params"][0].strip()
        role_id = cmd["params"][1].strip()
        value = cmd["params"][2].strip()

        # Restriction names
        if restriction not in current_restrictions[0]:
            return await message.channel.send("This restriction doesn't exist! 🔐")

        # Role id
        roles_ids = [str(role.id) for role in message.guild.roles]
        if role_id not in roles_ids:
            return await message.channel.send("There is no role with the specified id! 🔐")

        # Enable or disable
        if not (value == "0" or value == "1"):
            return await message.channel.send("You need to 0 to disable and 1 to enable the specified restriction! 🔐")

        value = bool(int(value))
        guild_id = str(message.guild.id)
        db = Config.mongo_client(guild_id)
        db_entry = db.restrictions.find_one({"id": role_id})
        if db_entry:
            db.restrictions.update_one(
                {"id": role_id},
                {'$set': {
                    restriction: value
                }
                },
                upsert=False
            )
            if role_id not in Restrictions.local_restrictions:
                Restrictions.local_restrictions[role_id] = db_entry
            Restrictions.local_restrictions[role_id][restriction] = value
        else:
            role_name = message.guild.get_role(int(role_id)).name
            entry = Restrictions.__default_restrictions(role_id, role_name, False)
            db.restrictions.insert_one(entry)

        await message.channel.send(f"Restrictions **{restriction}** updated to **{value}** for role **{role_id}**! 🔒")

    async def __show_restrictions(message: discord.Message, restrictions: dict) -> None:
        if message.content == "!restrictions":
            user_restrictions = util.read_dict(restrictions)
            await message.channel.send(f"Restrictions for __{message.author.display_name}__:\n{user_restrictions}")

    def __default_restrictions(role_id: str, role_name: str, is_allowed: bool) -> None:
        return {
            "id": role_id,
            "name": role_name,
            "restricted": True,
            "restrict": is_allowed,
            "image": is_allowed,
            "react": is_allowed,
            "music": is_allowed,
            "spam": is_allowed,
            "roles": is_allowed,
            "settings": is_allowed,
            "cleanup": is_allowed,
        }

    def __get_merged_restrictions(restrictions):
        result = {
            "restricted": True,
            "restrict": False,
            "image": False,
            "react": False,
            "music": False,
            "spam": False,
            "roles": False,
            "settings": False,
            "cleanup": False,
        }

        for restriction in restrictions:
            for key in restriction:
                if key in result:
                    if key == "restricted":
                        result[key] = result[key] and restriction[key]
                    result[key] = result[key] or restriction[key]

        return result
