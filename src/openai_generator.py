import discord
import io
import os
import rpyc

from app import App
from config import Config
from base_module import BaseModule
from discord.ext import tasks
from printer import Printer
from settings import Settings
from typing import final

import util

class OpenAIGenerator(BaseModule):
    is_connection_valid = True
    openai = {}
    image_conn = None
    prompt_conn = None
    results = []

    @final
    async def init() -> None:
        OpenAIGenerator.openai = Config.get_openai()
        OpenAIGenerator.establish_connection()

    @final
    async def on_message(message: discord.Message, _) -> None:
        if message.author == App.client.user:
            return

        await OpenAIGenerator.openai_completion(message)
        await OpenAIGenerator.openai_send_image(message)

    async def redeploy() -> None:
        # We ignore connection errors when resetting the bot since we'll reset the connection afterwards
        try:
            OpenAIGenerator.image_conn.root.redeploy()
        except BaseException:
            pass

        try:
            OpenAIGenerator.prompt_conn.root.redeploy()
        except BaseException:
            pass

    def establish_connection() -> None:
        try:
            OpenAIGenerator.image_conn = rpyc.connect("localhost", 18812, config={"allow_all_attrs": True})
            OpenAIGenerator.prompt_conn = rpyc.connect("localhost", 18813, config={"allow_all_attrs": True})
        except BaseException:
            OpenAIGenerator.is_connection_valid = False
            Printer.print_warning(
                "OpenAIGenerator couldn't connect. Make sure openai_images.py and openai_completion.py are running!"
            )

    async def openai_completion(message: discord.Message) -> None:
        if message.content.split(" ")[0] != "!prompt":
            return

        if not OpenAIGenerator.is_connection_valid:
            return await message.channel.send(content="OpenAIGenerator isn't working, can't process command! 💀")

        options = {
            "count": [1],
            "separator": (" ", 1),
            "nb_params_lower": "You need to enter a prompt so Beep Boop can respond! 🦆",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        sent_message = await message.channel.send(content="Currently generating an answer...")

        generate_func = rpyc.async_(OpenAIGenerator.prompt_conn.root.completion)
        result = generate_func(
            cmd["params"][0],
            util.get_user_id(message.author),
            Settings.local_settings[str(message.guild.id)]["prompt_risk_factor"]
        )
        OpenAIGenerator.results.append((sent_message, result))

        if len(OpenAIGenerator.results) == 1:
            OpenAIGenerator.result_callback.start()

    async def openai_send_image(message: discord.Message) -> None:
        command = message.content.split(" ")[0]
        payload = {
            "model": "dall-e-3",
            "user": util.get_user_id(message.author),
            "n": 1
        }

        if command == "!generate":
            payload["size"] = "1024x1024"
            payload["quality"] = "standard"
        elif command == "!hd":
            if message.author.name != os.getenv("ADMIN", ""):
                return await message.channel.send(content="You do not have the rights for this command! 💀💀💀")
            payload["size"] = "1792x1024"
            payload["quality"] = "hd"
        else:
            return

        if not OpenAIGenerator.is_connection_valid:
            return await message.channel.send(content="OpenAIGenerator isn't connected, can't process command! 💀")

        options = {
            "count": [1],
            "separator": (" ", 1),
            "nb_params_lower": "You need to enter a prompt so Beep Boop can generate an image! 🪄",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        sent_message = await message.channel.send("Currently generating an image...")

        generate_func = rpyc.async_(OpenAIGenerator.image_conn.root.generate_image)

        payload["prompt"] = cmd["params"][0]
        result = generate_func(payload)
        OpenAIGenerator.results.append((sent_message, result))

        if len(OpenAIGenerator.results) == 1:
            OpenAIGenerator.result_callback.start()

    @tasks.loop(seconds=1)
    async def result_callback():
        for msg, result in OpenAIGenerator.results:
            if result.ready:
                if isinstance(result.value, tuple):
                    content, data, filename = result.value
                    attachments = [discord.File(io.BytesIO(data), filename)] if data else []
                    await msg.edit(content=content, attachments=attachments)
                else:
                    await msg.edit(content=result.value)

        OpenAIGenerator.results = [x for x in OpenAIGenerator.results if not x[1].ready]
        if not OpenAIGenerator.results:
            OpenAIGenerator.result_callback.stop()
