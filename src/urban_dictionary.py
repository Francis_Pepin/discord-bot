import discord
import util

from app import App
from base_module import BaseModule
from datetime import datetime
from discord import Colour, Embed
from pyurbandict import UrbanDict
from typing import final

class UrbanDictionary(BaseModule):
    @final
    async def on_message(message: discord.Message, _) -> None:
        if message.author == App.client.user:
            return

        await UrbanDictionary.__fetch_label_definition(message)

    @staticmethod
    async def __fetch_label_definition(message: discord.Message) -> None:
        if not (message.content.split(" ")[0] == "!urb"):
            return

        # Random search
        if (message.content == "!urb"):
            response = UrbanDict().search()
            return await UrbanDictionary.__send_definition_message(message, response)

        options = {
            "count": [1],
            "separator": (" ", 1),
            "nb_params_lower": "You need to enter a search term! 🔍",
        }
        cmd = await util.command_async(message, options)
        search_label = cmd["params"][0]

        response = UrbanDict(search_label).search()
        response.sort(key=lambda r: (r.thumbs_up / r.thumbs_down) if r.thumbs_down > 0 else 0, reverse=True)
        await UrbanDictionary.__send_definition_message(message, response)

    @staticmethod
    async def __send_definition_message(message: discord.Message, resp: dict) -> None:
        if (len(resp) == 0):
            return await message.channel.send("No definition was found, sorry!")

        entry = resp[0]
        embed = Embed(
            title=UrbanDictionary.__parse(entry.word),
            description=UrbanDictionary.__parse(entry.definition),
            color=Colour.random(),
            url=entry.permalink
        )

        embed.add_field(
            name="\n*Example*",
            value=UrbanDictionary.__parse(entry.example),
            inline=False
        )

        dt = datetime.strptime(entry.written_on, "%Y-%m-%dT%H:%M:%S.%fZ")
        formatted_date = dt.strftime("%d %B, %Y")

        embed.set_footer(
            text=f"by {entry.author} on {formatted_date}\n👍 {entry.thumbs_up}   —   👎 {entry.thumbs_down}"
        )

        try:
            await message.channel.send(embed=embed)
        except BaseException:
            await message.channel.send("Couldn't retrieve that definition!")

    @staticmethod
    def __parse(response_string: str) -> str:
        return response_string.replace("[", "").replace("]", "")
