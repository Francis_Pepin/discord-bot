import asyncio
import discord
import util
import validators

from app import App
from base_module import BaseModule
from discord import Colour, Embed, FFmpegPCMAudio
from printer import Printer
from typing import Optional, Tuple, final
from youtubesearchpython import VideosSearch
from yt_dlp import YoutubeDL

class Music(BaseModule):
    queues = {}
    current_songs = {}

    @final
    async def on_message(message: discord.Message, restrictions: dict) -> None:
        if not restrictions["music"] and restrictions["restricted"]:
            return

        is_command = message.content.startswith("!")
        command_name = message.content.split(" ")[0].replace("!", "")
        music_function_name = f"music_{command_name}"
        if is_command and music_function_name in dir(Music):
            music_function = getattr(Music, music_function_name)
            await music_function(message)

    @final
    async def on_voice_state_update(before: discord.VoiceState, *_) -> None:
        if before.channel:
            vc = next((vc for vc in App.client.voice_clients if vc.channel.id == before.channel.id), None)
            if len(before.channel.members) < 2 and vc:
                print(f"Disconnecting Beep Boop from server : {vc.guild.name}")
                await Music.clear_internal(vc.guild)
                await vc.disconnect()

    async def music_stop(message: discord.Message) -> None:
        await message.channel.send("Stopping audio and getting rid of the queue! 🗑️")
        await Music.clear_internal(message.guild)

    async def music_skip(message: discord.Message) -> None:
        Music.__reset_current_song(message.guild.id)
        await message.channel.send("Skipping to next song! ⏭️")
        message.guild.voice_client.stop()

    async def music_pause(message: discord.Message) -> None:
        return message.guild.voice_client.pause()

    async def music_play(message: discord.Message, queue_song: Optional[str] = None) -> None:
        options = {
            "count": [0, 1],
            "separator": (" ", 1),
            "custom_content": queue_song if queue_song else message.content,
            "nb_params_lower": "You need to enter a url! 🔍",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        title = ""
        url = ""
        if cmd["params"]:
            if validators.url(cmd["params"][0]):
                url = cmd["params"][0]
            else:
                url = Music.__find_video_with_search_term(cmd)
                if not url:
                    return await message.channel.send(f"No results for this search! 🐳")
        else:
            url, title = await Music.__validate_attachment(message)
            if not url:
                return await message.channel.send(f"You need to link a file or add a url as a parameter! 📜")

        # Download audio from url
        is_playing = message.guild.voice_client and message.guild.voice_client.is_playing()
        queue = Music.__get_queue(message.guild.id)
        if is_playing or (len(queue) > 0 and not queue_song):
            await Music.__add_song_to_queue(cmd, queue, message, url, title)
            return

        if not message.guild.voice_client:
            is_in_voice_channel = await Music.music_join(message)
            if not is_in_voice_channel:
                return

        if message.guild.voice_client and not message.guild.voice_client.is_playing():
            await Music.__play_audio(message, url)

    async def music_repeat(message: discord.Message) -> None:
        options = {
            "count": [1],
            "separator": (" ", 1),
            "nb_params_lower": "You need to enter a url and a number of repetitions! 🔍",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        title = ""
        url = ""
        if cmd["params"]:
            # Accepts command separated by spaces or "|"
            if "|" in cmd["params"][0]:
                potential_url = cmd["params"][0].split("|")[1].replace(" ", "")
            else:
                potential_url = cmd["params"][0].split(" ", 1)[0]

            if validators.url(potential_url):
                url = potential_url
            else:
                url, title = await Music.__validate_attachment(message)
                if not url:
                    url = Music.__find_video_with_search_term(cmd)
                    if not url:
                        return await message.channel.send(f"No results for this search and no audio attachment! 🐳")

        queue = Music.__get_queue(message.guild.id)

        try:
            nb_repeats = int(cmd["params"][0].split(" ")[-1])
            if nb_repeats < 2 or nb_repeats > 20:
                return await message.channel.send(f"Invalid amount of repetitions! Please choose a number between 2 and 20! 🧮")
        except BaseException:
            return await message.channel.send(f"Repetition parameter must be a number! 🧮")

        ydl_options = Music.__build_ydl_options()
        with YoutubeDL(ydl_options) as ydl:
            info = ydl.extract_info(url, download=False)
            title = info.get("title", None)

        for _ in range(nb_repeats):
            await Music.__add_known_song_to_queue(queue, message, url, title)

        embed = Embed(
            title="Songs enqueued!",
            description=f"💿 *{title}* 💿 has been added to the queue **{nb_repeats} times** from position " +
            f"**{len(queue) - nb_repeats + 1}** to position **{len(queue)}**",
            url=url,
            color=Colour.random()
        )
        await message.channel.send(embed=embed)

        if not message.guild.voice_client:
            print("Joining voice client!")
            is_in_voice_channel = await Music.music_join(message)
            if not is_in_voice_channel:
                return

        if message.guild.voice_client and not message.guild.voice_client.is_playing():
            url, _ = queue.pop(0)
            await Music.music_play(message, queue_song=(f"!play {url}"))

    async def music_resume(message: discord.Message) -> None:
        return message.guild.voice_client.resume()

    async def music_join(message: discord.Message) -> None:
        if message.author.voice:
            channel = message.author.voice.channel

            if message.guild.afk_channel and channel.id == message.guild.afk_channel.id:
                await message.channel.send("Beep Boop doesn't want to join an AFK channel! 🍰")
                return False

            if not message.guild.voice_client:
                await channel.connect()
                await message.author.guild.change_voice_state(channel=channel, self_deaf=True)
                Printer.print_confirmation("Voice channel joined!")

            await message.channel.send("Beep Boop joined! 🎏")
        else:
            await message.channel.send("You have to be in a voice channel! 🍰")
        return bool(message.author.voice)

    async def music_leave(message: discord.Message) -> None:
        Music.__reset_current_song(message.guild.id)
        if message.guild.voice_client:
            await Music.clear_internal(message.guild)
            await message.guild.voice_client.disconnect()
            await message.channel.send("Beep Boop left! 👾")
        else:
            await message.channel.send("Beep Boop is not connected to a voice channel! 🛰️")

    async def __download(message: discord.Message, url: str) -> None:
        ydl_options = Music.__build_ydl_options()
        stream_url = ""
        try:
            with YoutubeDL(ydl_options) as ydl:
                info = ydl.extract_info(url, download=False)
                title = info.get("title", "Unknown")
                stream_url = info.get("url", None)
                duration = info.get("duration", None)
                views = info.get("view_count", None)

            embed = Embed(
                title=f"Now playing: 💽 *{title}* 💽",
                description=f"Duration: *{util.format_time(duration, False)}*\nViews: *{views}*" if views else "",
                url=url,
                color=Colour.random()
            )
            await message.channel.send(embed=embed)
            Music.current_songs[message.guild.id] = embed
        except Exception as e:
            Printer.print_error(e)
            await message.channel.send(f"⚠️ Invalid URL! ⚠️")

        return stream_url

    async def clear_internal(guild) -> None:
        Music.__reset_current_song(guild.id)
        queue = Music.__get_queue(guild.id)
        queue.clear()
        Music.queues[guild.id] = queue

        if guild.voice_client and guild.voice_client.is_playing():
            guild.voice_client.stop()

    async def music_queue(message: discord.Message) -> None:
        queue = Music.__get_queue(message.guild.id)
        if queue:
            embed_list = []
            embed = Embed(title=f"🎧 Queue 🎧", description="", color=Colour.random())
            for song_id, song in enumerate(queue):
                song_entry = f"**({song_id + 1})** *{song[1]}*\n"
                if len(embed.description + song_entry) < util.MAX_EMBED_LENGTH:
                    embed.description += song_entry
                else:
                    embed_list.append(embed)
                    embed = Embed(
                        title=f"🎧 Queue (part {len(embed_list) + 1}) 🎧",
                        description="",
                        color=Colour.random()
                    )
                    embed.description += song_entry

            embed_list.append(embed)

            for embed_entry in embed_list:
                await message.channel.send(embed=embed_entry)
        else:
            await message.channel.send("The queue is empty! 🐚")

    async def music_remove(message: discord.Message) -> None:
        queue = Music.__get_queue(message.guild.id)
        if queue:
            words = message.content.split(" ", 1)
            try:
                index = int(words[1]) - 1
                value = queue[index]
                queue.remove(value)
                await message.channel.send(f"Song 💿 *{value[1]}* 💿 removed! 🪐")
                Music.queues[message.channel.id] = queue
            except Exception:
                error_message = f"Invalid position! Please remove from first position to last (**1** to **{len(queue)}**) 🤡"
                await message.channel.send(error_message)
        else:
            await message.channel.send("The queue is empty! Nothing to remove! 🤔")

    async def music_swap(message: discord.Message) -> None:
        options = {
            "count": [2],
            "separator": (" ", 2),
            "nb_params_lower": "You need to enter a url! 🔍",
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        try:
            first_song_index = int(cmd["params"][0]) - 1
            second_song_index = int(cmd["params"][1]) - 1
        except BaseException:
            return await message.channel.send("Parameters are invalid! 🥵")

        is_valid_indexes = first_song_index in range(0, len(Music.queues[message.guild.id])) and \
            second_song_index in range(0, len(Music.queues[message.guild.id]))

        if not is_valid_indexes:
            return await message.channel.send("A song index is out of range! 💂")

        if first_song_index == second_song_index:
            return await message.channel.send("Congrats, you've just achieved nothing at all! 🎆")

        queue = Printer.swap_array_items(Music.queues[message.guild.id], first_song_index, second_song_index)
        return await message.channel.send(f"Song 💿 *{queue[second_song_index][1]}* 💿 has been swapped with 💿 " +
                                          f"*{queue[first_song_index][1]}* 💿 from position **{first_song_index + 1}** " +
                                          f"to position **{second_song_index + 1}**! 🔀")

    async def music_song(message: discord.Message) -> None:
        if Music.current_songs and Music.current_songs[message.guild.id]:
            await message.channel.send(embed=Music.current_songs[message.guild.id])
        else:
            await message.channel.send("No song is currently playing! 🎺")

    # Private methods
    def __find_video_with_search_term(cmd: dict) -> str:
        search = VideosSearch(cmd["params"][0], limit=1)
        if search.result()["result"]:
            return search.result()["result"][0]["link"]
        return ""

    async def __add_song_to_queue(cmd: dict, queue: dict, message: discord.Message, url: str, title: str) -> None:
        try:
            ydl_options = Music.__build_ydl_options()
            with YoutubeDL(ydl_options) as ydl:
                if cmd["params"]:
                    title = ydl.extract_info(url, download=False).get("title", None)
                queue.append((url, title))
                Music.queues[message.channel.id] = queue
                embed = Embed(
                    title="Song enqueued!",
                    description=f"💿 *{title}* 💿 has been added to the queue at position **{len(queue)}**",
                    url=url,
                    color=Colour.random()
                )
                await message.channel.send(embed=embed)
        except Exception:
            await message.channel.send("Invalid URL!")

    async def __add_known_song_to_queue(queue, message: discord.Message, url: str, title: str) -> None:
        queue.append((url, title))
        Music.queues[message.channel.id] = queue

    async def __play_audio(message: discord.Message, url: str) -> None:
        stream_url = await Music.__download(message, url)
        print(f"Playing audio from streaming url : {stream_url}")
        audio = FFmpegPCMAudio(stream_url, before_options='-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5')
        try:
            message.guild.voice_client.play(
                audio,
                after=lambda _: asyncio.run_coroutine_threadsafe(Music.__after(message), App.client.loop)
            )
        except Exception:
            error_message = "There was an issue streaming this url! To fix this issue, call the following commands " \
                            "in order: !stop, !leave, !play ``URL``. If the bot stays in the channel somehow, make sure to " \
                            "disconnect it manually to reset it's state. If it still doesn't work, listen to something else 🙈"
            await message.channel.send(error_message)

    def __reset_current_song(guild_id: int) -> None:
        Music.current_songs[guild_id] = None

    # NOTE : Do not change the parameters of this callback function
    async def __after(message: discord.Message) -> None:
        Music.__reset_current_song(message.guild.id)
        queue = Music.__get_queue(message.guild.id)
        if queue:
            url, _ = queue.pop(0)
            Music.queues[message.channel.id] = queue
            print(f"Next song : {url}")
            await Music.music_play(message, queue_song=(f"!play {url}"))

    async def __validate_attachment(message: discord.Message) -> Tuple[Optional[str], Optional[str]]:
        if not message.attachments:
            return None, None

        attachement = message.attachments[0]
        if not attachement.content_type:
            attachement.content_type = ""

        if "audio" in attachement.content_type or ".m4p" in attachement.filename:
            return attachement.url, attachement.filename
        else:
            await message.channel.send(f"Content type is not supported! 🪂")
            return None, None

    def __get_queue(id) -> dict:
        if id not in Music.queues:
            Music.queues[id] = []

        return Music.queues[id]

    def __build_ydl_options() -> dict:
        return {
            "format": "bestaudio",
            "postprocessors": [{
                "key": "FFmpegExtractAudio",
                "preferredcodec": "wav",
                "preferredquality": "192",
            }],
            "noplaylist": True,
            "nocheckcertificate": True,
            "keepvideo": False,
        }
