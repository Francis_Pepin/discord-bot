import asyncio
import discord
import threading
import util

from app import App
from base_module import BaseModule
from config import Config
from datetime import datetime
from printer import Printer
from pymongo import MongoClient
from pytz import timezone
from typing import final

class Uptime(BaseModule):
    user_states = {}
    last_stored_user_states = {}

    @final
    async def init() -> None:
        await Uptime.handle_bot_cycle(App.client)

        # Storing uptime on 15 minutes delay
        delay = 900
        Uptime.__periodic_uptime_storage_loop(App.client, delay)

    @final
    async def on_message(message: discord.Message, _) -> None:
        if message.author == App.client.user:
            return

        await Uptime.__send_total_uptime(message)
        await Uptime.__send_current_uptime(message)
        await Uptime.__send_leaderboard(message)
        await Uptime.__store_uptime(message, App.client)

    @final
    async def on_voice_state_update(before: discord.VoiceState, after: discord.VoiceState, member: discord.Member) -> None:
        user = util.get_user_key(member)

        if (before.channel is None and after.channel is not None):
            Uptime.connect_user_state(user)
        elif (before.channel is not None and after.channel is None):
            Uptime.disconnect_user_state(user, str(before.channel.guild.id))

    async def handle_bot_cycle(client: discord.Client, is_storing: bool = False) -> None:
        for guild in client.guilds:
            for channel in guild.voice_channels:
                for member_id in channel.voice_states:
                    try:
                        member_data = await guild.query_members(user_ids=[member_id])
                        member = str(member_data[0])
                        if is_storing:
                            Uptime.disconnect_user_state(member, str(guild.id))
                        else:
                            Uptime.connect_user_state(member)
                    except Exception:
                        Printer.print_error(f"No user with id {member_id}")

    def connect_user_state(user: str) -> None:
        if Uptime.user_states.get(user):
            Uptime.user_states.update({user: int(datetime.now().timestamp())})
        else:
            Uptime.user_states[user] = int(datetime.now().timestamp())

    def disconnect_user_state(user: str, guild_id: str) -> None:
        start_time = Uptime.last_stored_user_states.get(user)
        if not start_time:
            start_time = Uptime.user_states.get(user)
            if not start_time:
                start_time = datetime.now().timestamp()

        delta = int(datetime.now().timestamp()) - int(start_time)
        Uptime.user_states.update({user: None})
        Uptime.last_stored_user_states.update({user: None})

        # Store delta in db
        db = Config.mongo_client(guild_id)
        user_uptime = db.total_uptime.find_one({"id": user})
        if user_uptime is not None:
            Uptime.__update_total_uptime(user, delta, user_uptime, db)
        else:
            Uptime.__insert_total_uptime(user, delta, db)

    def __periodic_uptime_storage_loop(client: discord.Client, delay: int):
        asyncio.run_coroutine_threadsafe(Uptime.__periodic_uptime_storage(client), client.loop)
        threading.Timer(delay, Uptime.__periodic_uptime_storage_loop, (client, delay)).start()

    async def __periodic_uptime_storage(client: discord.Client) -> None:
        stored_items = 0
        for guild in client.guilds:
            db = Config.mongo_client(str(guild.id))
            for channel in guild.voice_channels:
                for member_id in channel.voice_states:
                    member_data = await guild.query_members(user_ids=[member_id])
                    user = str(member_data[0])

                    current_time = datetime.now().timestamp()
                    previous_time = Uptime.last_stored_user_states.get(user)
                    if not previous_time:
                        previous_time = Uptime.user_states.get(user)
                        if not previous_time:
                            previous_time = current_time

                    delta = int(current_time) - int(previous_time)
                    Uptime.last_stored_user_states[user] = current_time

                    # Store delta in db
                    user_uptime = db.total_uptime.find_one({"id": user})
                    if user_uptime is not None:
                        Uptime.__update_total_uptime(user, delta, user_uptime, db)
                    else:
                        Uptime.__insert_total_uptime(user, delta, db)

                    stored_items += 1

        if stored_items > 0:
            tz = timezone('Canada/Eastern')
            storage_time = datetime.now(tz).strftime("%A, %d %B %Y at %H:%M:%S Canada/Eastern time")
            print(f"Periodic uptime storage for {stored_items} user(s): {storage_time}")

    async def __send_current_uptime(message: discord.Message) -> None:
        if message.content != "!uptime current":
            return

        user = util.get_user_id(message.author)
        uptime = 0
        if Uptime.user_states.get(user):
            uptime = int(datetime.now().timestamp()) - Uptime.user_states.get(user)
        await message.channel.send(f"Current uptime: {util.format_time(uptime)} 🕑")

    async def __send_total_uptime(message: discord.Message) -> None:
        if message.content != "!uptime total":
            return

        user = util.get_user_key(message.author)
        db = Config.mongo_client(str(message.guild.id))
        user_uptime = db.total_uptime.find_one({"id": user})
        if user_uptime is not None:
            current_uptime = Uptime.last_stored_user_states.get(user)
            if not current_uptime:
                current_uptime = Uptime.user_states.get(user)

            current_uptime = int(datetime.now().timestamp()) - current_uptime if current_uptime else 0

            total = user_uptime["uptime"] + current_uptime
            await message.channel.send(f"Total time spent in voice calls: {util.format_time(total)} 🕗")
        else:
            await message.channel.send("There is no data for your user! 🐱‍👤")

    async def __store_uptime(message: discord.Message, client: discord.Client) -> None:
        if message.content != "!uptime store":
            return

        await Uptime.handle_bot_cycle(client, is_storing=True)
        await Uptime.handle_bot_cycle(client, is_storing=False)
        await message.channel.send("Total uptime stored for every user! 💾")

    async def __send_leaderboard(message: discord.Message) -> None:
        if "!uptime leaderboard" not in message.content:
            return

        limit = 25
        params = message.content.split(" ", 3)
        if len(params) > 2:
            try:
                limit = int(params[2])
            except BaseException:
                return await message.channel.send("Invalid parameter! Please use a valid number" +
                                                  " as a parameter or use the default uptime leaderboard command! 🤡")

        table_rows = ""
        db = Config.mongo_client(str(message.guild.id))
        for rank, uptime in enumerate(db.total_uptime.find().sort("uptime", -1).limit(limit)):
            table_rows += f"{rank + 1}. {uptime['id']} - **{util.format_time(uptime['uptime'])}**\n"

        await util.send_table(message, "Uptime leaderboard", table_rows.strip(), True)

    def __update_total_uptime(user, delta, data, db: MongoClient) -> None:
        db.total_uptime.replace_one(
            {"id": user},
            {"id": user, "uptime": data["uptime"] + delta},
            upsert=False
        )

    def __insert_total_uptime(user, delta, db: MongoClient) -> None:
        db.total_uptime.insert_one({"id": user, "uptime": delta})
