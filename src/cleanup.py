import discord
import os
import util

from app import App
from base_module import BaseModule
from typing import final

class Cleanup(BaseModule):
    max_messages: int = 100
    messages: dict[int, list[discord.Message]] = {}

    @final
    async def on_message(message: discord.Message, _) -> None:
        guild_id = message.guild.id
        if guild_id not in Cleanup.messages:
            Cleanup.messages[guild_id] = []

        Cleanup.__add_to_cleanup_if_valid(message)
        await Cleanup.__cleanup_command(message, Cleanup.messages[guild_id])
        await Cleanup.__redeploy(message)

    def __add_to_cleanup_if_valid(message: discord.Message) -> None:
        is_bot = message.author == App.client.user
        is_command = message.content and message.content[0] == "!"
        is_not_cleanup_command = message.content.split(" ")[0] != "!cleanup"

        should_allow_message_deletion = is_bot or (is_command and is_not_cleanup_command)

        if should_allow_message_deletion:
            Cleanup.__add_message(message, Cleanup.messages[message.guild.id])

    def __add_message(message: discord.Message, message_list: list[discord.Message]) -> None:
        message_list.append(message)
        message_list.sort(key=lambda msg: msg.created_at)

        if len(message_list) > Cleanup.max_messages:
            message_list.pop(0)

    async def __cleanup_command(message: discord.Message, message_list: list[discord.Message]) -> None:
        if (message.content.split(" ")[0] != "!cleanup"):
            return

        options = {
            "count": [0, 1],
            "separator": (" ", 1),
        }
        cmd = await util.command_async(message, options)
        if not cmd["is_valid_params"]:
            return

        if cmd["params"] and not cmd["params"][0].isnumeric():
            return await message.channel.send("You can only use this command with no parameter or a numeric parameter! 🔢")

        channel = message.channel
        user = message.author
        try:
            await message.delete()
        except BaseException:
            message.channel.send("Can't clean up anymore! 📉")

        if not cmd["params"]:
            return await Cleanup.__cleanup(message_list, user, channel)

        if cmd["params"][0].isnumeric():
            await Cleanup.__cleanup(message_list, user, channel, nb_messages=int(cmd["params"][0]))

    async def __cleanup(message_list: list[discord.Message], user: discord.User,
                        channel: discord.GroupChannel, nb_messages: int = max_messages) -> None:
        message_list.sort(key=lambda msg: msg.created_at)
        filtered_messages = [msg for msg in message_list if msg.channel.id == channel.id][-nb_messages:]

        await channel.delete_messages(
            filtered_messages,
            reason=f"Manual clean up by {user.id} using !cleanup command"
        )

        deleted_ids = {msg.id for msg in filtered_messages}
        message_list[:] = [msg for msg in message_list if msg.id not in deleted_ids]

    async def __redeploy(message: discord.Message) -> None:
        if message.content.split(" ")[0] == "!redeploy" and message.author.name == os.getenv("ADMIN", ""):
            await message.channel.send(content="Redeploying bot!")
            print("Killing main process")
            os._exit(1)
