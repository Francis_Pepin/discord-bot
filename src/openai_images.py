import aiohttp
import asyncio
import config
import openai
import os
import rpyc
import sys

from openai import OpenAI
from printer import Printer
from rpyc.utils.server import ThreadedServer

class ImagesService(rpyc.Service):
    def __init__(self):
        config
        openai.api_key = os.getenv("OPENAI_API_KEY", None)

    def exposed_redeploy(self):
        print("Killing image generator")
        return sys.exit()

    def exposed_generate_image(self, payload):
        return asyncio.run(self.internal_generate_image(payload))

    async def internal_generate_image(self, payload):
        try:
            client = OpenAI(
                api_key=os.getenv("OPENAI_API_KEY", None)
            )
            response = client.images.generate(
                model=payload["model"],
                prompt=payload["prompt"],
                user=payload["user"],
                size=payload["size"],
                quality=payload["quality"],
                n=payload["n"]
            )

            async with aiohttp.ClientSession() as session:
                async with session.get(response.data[0].url) as resp:
                    if resp.status != 200:
                        raise Exception

                    data = await resp.read()
                    filename = f"{payload['prompt']}.png"
                    return ("", data, filename)
        except Exception as e:
            print(e)
            content = "Couldn't generate an image! 💥"
            match type(e):
                case openai.AuthenticationError:
                    content = "The bot couldn't be athentificated, please notify the bot's owner! 💥"
                case openai.BadRequestError:
                    content = "This command was rejected due to a bad request, please notify the bot's owner! 💥"
                case openai.BadRequestError:
                    content = "This command was rejected due to permission issues, please notify the bot's owner! 💥"

            return (content, None, None)


if __name__ == "__main__":
    Printer.print_confirmation("OpenAI images server is running!")
    server = ThreadedServer(ImagesService, port=18812)
    server.start()
