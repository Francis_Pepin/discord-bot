<h1>About this bot</h1>

This discord bot was made by Francis Pepin. For now, it's purpose is only meant for specific private discord servers. However, all of the commands should work for any server and this bot might be released for a wider scale in the future.

The bot is running in a [Docker](https://www.docker.com/) image on a local server (thanks [samichoulo911](https://gitlab.com/samichoulo911)) and uses [MongoDB](https://www.mongodb.com/) as a database. Some commands were also made possible with the use of [OpenAI's API](https://beta.openai.com/docs/api-reference/introduction). Python 12 is required to run this project locally, as well as the .env file and ffmpeg for any command related to the music. You can find the required environment variables in config.py, or in the following .env example.

<h1>Project setup</h1>

<h3>Essentials</h3>

To install dependencies, run the following command:

`pip install -r requirements.txt`

To use this code on your own bot, create a .env file with following environment variables and replace the fields with your bot's token, your OpenAI API key and your MongoDB information:

```
BOT_TOKEN=(YourDiscordBotToken)
DEV_TOKEN=(YourDiscordBotToken)
ADMIN=(YourDiscordAccountUniqueName)
MONGODB_URL=(YourDatabaseURL)
MONGODB_USERNAME=(YourDatabaseUsername)
MONGODB_PASSWORD=(YourDatabasePassword)
OPENAI_API_KEY=(YourOpenAIApiKey)
IS_SUPPORTING_EMOJIS=True or False depending on terminal
IS_PROD=True
```

To run the discord bot locally from the root folder, run the following command:

`python src/main.py`

If you are on windows and you have Windows Terminal, you can execute `./run` to run the bot, or also run the `Beep Boop: Run` task.

<h3>Dev tools</h3>

You can install the dev dependencies using the following command:

`pip install -r requirements.dev.txt`

There are tasks available in `.vscode/tasks.json` to lint using [Pycodestyle](https://pycodestyle.pycqa.org/en/latest/index.html) and to format using [Autopep8](https://pypi.org/project/autopep8/). You can run these tasks through VSCode or simply run the command lines directly in a terminal at the root of the project. The linter uses the rules indicated in `setup.cfg`, which can be configured like shown [here](https://pycodestyle.pycqa.org/en/latest/intro.html#configuration). The formatter uses the options indicated in `pyproject.toml`.

<h1>List of commands</h1>

<h4>Restriction commands</h4>

| Command                 | Description |
| ----------------------- | ----------------------- |
|**!restrictions** | Retrieves the list of restrictions set for the author of this command. |
|**!restrict** \| *<_restriction_>* \| *<_role_>* \| *<_number_>* | Assign a boolean value to the specified restriction of the specified role id. Restriction list is the following : restricted, restrict, image, react, music, spam, roles, settings. The number is 0 for false and 1 for true. The role id can get fetched with the command `!server_roles`. |

<h4>Settings commands</h4>

| Command           | Description |
| ----------------------- | ----------------------- |
|**!settings** | Retrieves the list of settings set for the current discord server. |
|**!settings** *<_setting_>* *<_number_>* | Assign a boolean value or a float value to the specified setting for the server. Settings list is the following : safe_search, dm_help_messages, change_server_icon_daily, enable_specific_channels, prompt_risk_factor. For boolean values, the number is 0 for false and 1 for true. For float values, the number is between 0.0 and 1.0. |
|**!enable_channel** | Enables commands in the text channel where this command was written.|
|**!disable_channel** | Disables commands in the text channel where this command was written.|
|**!redeploy** | Kills the processes on which the bot runs. This command only works the `ADMIN` discord user id set in the `.env`.|


<h4>Image commands</h4>

| Command         | Description |
| ----------------------- | ----------------------- |
|**!image** *<search_term>* | Returns a random image from google corresponding to the `search_term` input. You can also add the character `*` at the end of the `search_term` to send the image anonymously.|
|**!gif** *<search_term>* | Returns a random gif from google corresponding to the `search_term` input. You can also add the character `*` at the end of the `search_term` to send the gif anonymously.|
|**!server_icon** | Returns the server's icon in high resolution.|
|**!icon_add** *<search_term>* | Adds a `search_term` to the list of terms used when the `change_server_icon_daily` setting is activated. |
|**!icon_remove** | Removes a `search_term` to the list of terms used when the `change_server_icon_daily` setting is activated. |
|**!icon_list** | View the list of terms used when the `change_server_icon_daily` setting is activated. |
|**!generate** *<prompt>* | Generate a 1024x1024 image with the given `prompt`. Makes requests to [OpenAI](https://beta.openai.com/docs/api-reference/images) using [DALL·E 3](https://openai.com/dall-e-3) and sends the generated image url. Note that your discord username is sent as part of the author of the request.|
|**!hd** *<prompt>* | Generate a 1792x1024 image of high quality with the given `prompt`. The command is similar to `!generate`, but since it is costly to run, only the `ADMIN` discord username set in the `.env` can generate high resolution images.|

<h4>React commands</h4>

| Command                         | Description |
| ----------------------- | ----------------------- |
|**!react_add** \| *<_term_>* \| *<_emojis_>* | Adds a list of `emojis` that beep boop will react when the `term` is in a message.|
|**!react_remove** *<_term_>* | Deletes the emoji reactions for the specified existing `term`. |
|**!react_list** | Displays the list of terms with their assigned reaction emojis. |
|• Discord bot reacts an emoji if the message contains it's name|                                                     |
  
<h4>Music commands</h4>

| Command | Description |
| ----------------------- | ----------------------- |
|**!play** *<search_term>* | Makes Beep Boop play the audio of the most viewed video with the `search_term` on youtube. |
|**!play** *<youtube_url>* | Makes Beep Boop play the audio of a video specified with the `youtube_url`. |
|**!play** | Makes Beep Boop play the audio file attached to the message if the format is valid and it is under 50MB. |
|**!repeat** *<youtube_url>* *<nb_repetitions>*| Queues the song with `youtube_url` as a video url for `nb_repetitions` amount of times. Plays the song if no audio is currently playing.   |
|**!repeat** *<search_term>* *<nb_repetitions>*| Queues the song fetched using `search_term` to find the video on youtube for `nb_repetitions` amount of times. Plays the song if no audio is currently playing.   |
|**!repeat** *<nb_repetitions>*| Queues the audio file attached to the message for `nb_repetitions` amount of times. Plays the song if no audio is currently playing. The attachment must have a valid format and must be under 50MB. |
|**!song** | Displays the name of the song currently playing, as well as the youtube link. |
|**!skip** | Skips song and plays next song in queue. |
|**!stop** | Stops Beep Boop's audio output and clears the song queue. |
|**!pause** | Pauses Beep Boop's audio output.|
|**!resume** | Resumes Beep Boop's audio output. |
|**!join** | Makes Beep Boop join the voice channel you are currently in. |
|**!leave** | Makes Beep Boop leave the voice channel it is in. |
|**!queue** | Displays a list of the songs in the queue with their position. |
|**!remove** *<_position_>*| Removes a song from the queue at the specified `position`.|
|**!swap** *<_first_position_>* *<_second_position_>*| Swap the two indexes `first_position` and `second_position` of songs in the queue.|


<h4>Odds commands</h4>

| Command             | Description |
| ----------------------- | ----------------------- |
|**!random** | Generates a random number from 1 to 10. |
|**!random** *<_to_number_>* | Generates a random number from 1 to `to_number` |
|**!random** *<_from_number_>* *<_to_number_>* | Generates a random number from `from_number` to `to_number` |
|**!choose** \| *<_first_outcome_>* \| *<_second_outcome_>* | Randomly decides whether you should do the `fist_outcome` or `second_outcome`. Great for making terrible life decisions. You can also choose from 2 to 10 different outcomes by adding more parameters. |
|**!russian_roulette** *<_punishment_>* | Play russian roulette (1/6 chances of getting shot) with the chosen `punishment` if you lose. |
|**!rr** *<_punishment_>*| Play russian roulette (1/6 chances of getting shot) with the chosen `punishment` if you lose. Shorter syntax. |
|**!russian_roulette** *list* or **!rr** *list* | Display the list of possible `punishments` -> kick, ban, disconnect, rename, deafen, mute, mix (rename/mute/deafen)|

<h4>Misc commands</h4>

| Command               | Description                                                          |
| ----------------------- | ----------------------- |
|**!urb** | Sends a random definition from [Urban Dictionary](https://www.urbandictionary.com/). |
|**!urb** *<_term_>* | Sends the definition from [Urban Dictionary](https://www.urbandictionary.com/) with the specified `term`. |
|**!wiki** | Sends the summary of a random [Wikipedia](https://en.wikipedia.org/) page. |
|**!wiki** *<_term_>* | Sends the summary of the [Wikipedia](https://en.wikipedia.org/) page with the specified `term`. |
|**!prompt** *<_prompt_>* | Replies to a `prompt` sent. Makes requests to [OpenAI](https://beta.openai.com/docs/api-reference/completions) and sends the text response. Uses the `prompt_risk_factor` setting, which helps provide more creative answers for high values and stable answers for low values (defaults to 0.2). Note that your discord username is sent as part of the author of the request. |

<h4>Spam commands</h4>

| Command               | Description |
| ----------------------- | ----------------------- |
|**!spam** \| *<_text_>* \| *<nb_of_repeats <= 20>* | Spams the specified `text` as many times as specified in `nb_of_repeats`. Maximum 20 repeats per command, otherwise it gets excessive.. |
|**!tts** \| *<_text_>* \| *<nb_of_repeats <= 20>* | Spams the specified `text` in text to speech as many times as specified in `nb_of_repeats`. Maximum 20 repeats per command, otherwise it gets excessive..
|**!stimer** *<_amount_>* *<_message_>* | Sets a timer with specified `amount` in seconds to send a `message`. |
|**!mtimer** *<_amount_>* *<_message_>* | Sets a timer with specified `amount` in minutes to send a `message`. |
|**!htimer** *<_amount_>* *<_message_>* | Sets a timer with specified `amount` in hours to send a `message`. |

<h4>Cleanup commands</h4>

| Command               | Description |
| ----------------------- | ----------------------- |
|**!cleanup** | Deletes the last 20 saved messages sent by the bot or containing a command. |
|**!cleanup** *<_nb_messages_ <= 20>* | Deletes the last `nb_messages` amount of saved messages sent by the bot or containing a command. Maximum of 20 deletions per command, as too many cleanups may cause the delete calls get blocked by the discord API. |
  
<h4>Stats commands</h4>

| Command             | Description |
| ----------------------- | ----------------------- |
|**!stats** *emotes* | Displays a list of custom servers emotes and global emotes with the amount of times they have been typed at least once in a message. Does not count bot messages.|
|**!stats** *reacts* | Displays a list of custom servers emotes and global emotes with the amount of times they have been reacted onto messages. Does not count bot reactions.|
|**!stats** *<_emoji_>* | Displays the amount of times the custom or global `<emoji>` has been typed and reacted with in the server.
|**!uptime** *current* | Displays the current amount of time you have been in a voice call in this server.
|**!uptime** *total* | Displays the total amount of time you have been in a voice call in this server.
|**!uptime** *store* | Stores the total amount of time in a voice call for every user connected in a voice channel. This is mainly used for maintenance purposes when the bot has to be reset because the bot automatically adds the current uptime when a user disconnects.
|**!uptime** *leaderboard* | Displays the total amount of time in voice calls in descending order for the top 25 users that previously connected to voice chat.
|**!uptime** *leaderboard* *<_user_count_>*| Displays the total amount of time in voice calls in descending order for specified count of users that previously connected to voice chat.

<h4>Role commands</h4>

| Command              | Description |
| ----------------------- | ----------------------- |
|**!roles** | Displays the list of roles held by the current user. The indexes *<_role_index_>* shown with the role names should be the ones used to modify the role's attributes with the other commands.|
|**!server_roles** | Displays the list of roles available in the server. The number *<_role_>* shown with the role names should be the one used to modify the role's restrictions with the !restrict command|
|**!colors** | Displays the list of color names that can be used to change a role's color. For more information, see the [DiscordPy Colour documentation](https://discordpy.readthedocs.io/en/stable/api.html#discord.Colour).|
|**!role_name** *<_role_index_>* *<_new_name_>*| Changes the name of the role with the index `<role_index>` to the specified `<new_name>`.
|**!role_color** *<_role_index_>* *<_new_color_>*| Changes the color of the role with the index `<role_index>` to the specified `<new_color>`. The attribute `<new_color>` has many accepted values. It is possible to get a predetermined set of colors using the `!colors` command. The RGB values are accepted with spaces in between (ex:`<new_color> = 255 255 0`). Hexadecimal values are accepted as well in many formats and is not case sensitive (ex: `<new_color> = #FFFFFF`,`<new_color> = #ff00ff`, `<new_color> = FFFFFF`, `<new_color> = 0xFFFFFF`).
